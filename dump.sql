-- phpMyAdmin SQL Dump
-- version 3.5.5
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Apr 09, 2013 at 06:19 PM
-- Server version: 5.5.24-log
-- PHP Version: 5.4.3

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `newdawn`
--

-- --------------------------------------------------------

--
-- Table structure for table `blacklist`
--

CREATE TABLE IF NOT EXISTS `blacklist` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `subnetwork` varchar(11) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=cp1251 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `characters`
--

CREATE TABLE IF NOT EXISTS `characters` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `PlayerID` int(11) NOT NULL,
  `Name` varchar(24) DEFAULT NULL,
  `RegisteredT` int(11) NOT NULL,
  `Dead` int(11) NOT NULL DEFAULT '0',
  `Sex` int(11) DEFAULT NULL,
  `Race` int(11) DEFAULT NULL,
  `Age` int(11) NOT NULL DEFAULT '0',
  `MinutesOn` int(11) DEFAULT '0',
  `JailTime` int(11) NOT NULL DEFAULT '0',
  `JailCell` int(11) NOT NULL DEFAULT '0',
  `Premium` int(11) NOT NULL DEFAULT '0',
  `Money` int(11) DEFAULT '500',
  `Bank` int(11) NOT NULL DEFAULT '0',
  `Job` int(11) NOT NULL DEFAULT '0',
  `Org` int(11) DEFAULT '0',
  `Rank` int(11) DEFAULT '0',
  `Skin` int(11) DEFAULT NULL,
  `LastX` int(11) DEFAULT '0',
  `LastY` int(11) DEFAULT '0',
  `LastZ` int(11) DEFAULT '0',
  `LastFA` float NOT NULL DEFAULT '0',
  `LastInt` int(11) DEFAULT '0',
  `LastVW` int(11) DEFAULT '0',
  `EquippedMelee` int(11) NOT NULL DEFAULT '0',
  `EquippedMisc` int(11) NOT NULL DEFAULT '0',
  `EquippedExplosives` int(11) NOT NULL DEFAULT '0',
  `EquippedGun1` int(11) NOT NULL DEFAULT '0',
  `EquippedAmmo1` int(11) NOT NULL DEFAULT '0',
  `EquippedSerial1` int(11) NOT NULL DEFAULT '0',
  `EquippedGun2` int(11) NOT NULL DEFAULT '0',
  `EquippedAmmo2` int(11) NOT NULL DEFAULT '0',
  `EquippedSerial2` int(11) NOT NULL DEFAULT '0',
  `EquippedPhone` int(11) NOT NULL DEFAULT '0',
  `PhoneNumber` int(11) NOT NULL DEFAULT '0',
  `DrugAddictLevel` int(3) NOT NULL DEFAULT '0',
  `InvItem0` int(11) NOT NULL DEFAULT '0',
  `InvQuantity0` int(11) NOT NULL DEFAULT '0',
  `InvSerial0` int(11) NOT NULL DEFAULT '0',
  `InvItem1` int(11) NOT NULL DEFAULT '0',
  `InvQuantity1` int(11) NOT NULL DEFAULT '0',
  `InvSerial1` int(11) NOT NULL DEFAULT '0',
  `InvItem2` int(11) NOT NULL DEFAULT '0',
  `InvQuantity2` int(11) NOT NULL DEFAULT '0',
  `InvSerial2` int(11) NOT NULL DEFAULT '0',
  `InvItem3` int(11) NOT NULL DEFAULT '0',
  `InvQuantity3` int(11) NOT NULL DEFAULT '0',
  `InvSerial3` int(11) NOT NULL DEFAULT '0',
  `InvItem4` int(11) NOT NULL DEFAULT '0',
  `InvQuantity4` int(11) NOT NULL DEFAULT '0',
  `InvSerial4` int(11) NOT NULL DEFAULT '0',
  `InvItem5` int(11) NOT NULL DEFAULT '0',
  `InvQuantity5` int(11) NOT NULL DEFAULT '0',
  `InvSerial5` int(11) NOT NULL DEFAULT '0',
  `InvItem6` int(11) NOT NULL DEFAULT '0',
  `InvQuantity6` int(11) NOT NULL DEFAULT '0',
  `InvSerial6` int(11) NOT NULL DEFAULT '0',
  `InvItem7` int(11) NOT NULL DEFAULT '0',
  `InvQuantity7` int(11) NOT NULL DEFAULT '0',
  `InvSerial7` int(11) NOT NULL DEFAULT '0',
  `InvItem8` int(11) NOT NULL DEFAULT '0',
  `InvQuantity8` int(11) NOT NULL DEFAULT '0',
  `InvSerial8` int(11) NOT NULL DEFAULT '0',
  `InvItem9` int(11) NOT NULL DEFAULT '0',
  `InvQuantity9` int(11) NOT NULL DEFAULT '0',
  `InvSerial9` int(11) NOT NULL DEFAULT '0',
  `InvItem10` int(11) NOT NULL DEFAULT '0',
  `InvQuantity10` int(11) NOT NULL DEFAULT '0',
  `InvSerial10` int(11) NOT NULL DEFAULT '0',
  `InvItem11` int(11) NOT NULL DEFAULT '0',
  `InvQuantity11` int(11) NOT NULL DEFAULT '0',
  `InvSerial11` int(11) NOT NULL DEFAULT '0',
  `InvItem12` int(11) NOT NULL DEFAULT '0',
  `InvQuantity12` int(11) NOT NULL DEFAULT '0',
  `InvSerial12` int(11) NOT NULL DEFAULT '0',
  `InvItem13` int(11) NOT NULL DEFAULT '0',
  `InvQuantity13` int(11) NOT NULL DEFAULT '0',
  `InvSerial13` int(11) NOT NULL DEFAULT '0',
  `InvItem14` int(11) NOT NULL DEFAULT '0',
  `InvQuantity14` int(11) NOT NULL DEFAULT '0',
  `InvSerial14` int(11) NOT NULL DEFAULT '0',
  `Description_Look` varchar(128) NOT NULL DEFAULT '',
  `Description_Look2` varchar(128) NOT NULL DEFAULT '',
  `Description_Voice` varchar(24) NOT NULL DEFAULT '',
  `status` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=cp1251 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `characters`
--

INSERT INTO `characters` (`id`, `PlayerID`, `Name`, `RegisteredT`, `Dead`, `Sex`, `Race`, `Age`, `MinutesOn`, `JailTime`, `JailCell`, `Premium`, `Money`, `Bank`, `Job`, `Org`, `Rank`, `Skin`, `LastX`, `LastY`, `LastZ`, `LastFA`, `LastInt`, `LastVW`, `EquippedMelee`, `EquippedMisc`, `EquippedExplosives`, `EquippedGun1`, `EquippedAmmo1`, `EquippedSerial1`, `EquippedGun2`, `EquippedAmmo2`, `EquippedSerial2`, `EquippedPhone`, `PhoneNumber`, `DrugAddictLevel`, `InvItem0`, `InvQuantity0`, `InvSerial0`, `InvItem1`, `InvQuantity1`, `InvSerial1`, `InvItem2`, `InvQuantity2`, `InvSerial2`, `InvItem3`, `InvQuantity3`, `InvSerial3`, `InvItem4`, `InvQuantity4`, `InvSerial4`, `InvItem5`, `InvQuantity5`, `InvSerial5`, `InvItem6`, `InvQuantity6`, `InvSerial6`, `InvItem7`, `InvQuantity7`, `InvSerial7`, `InvItem8`, `InvQuantity8`, `InvSerial8`, `InvItem9`, `InvQuantity9`, `InvSerial9`, `InvItem10`, `InvQuantity10`, `InvSerial10`, `InvItem11`, `InvQuantity11`, `InvSerial11`, `InvItem12`, `InvQuantity12`, `InvSerial12`, `InvItem13`, `InvQuantity13`, `InvSerial13`, `InvItem14`, `InvQuantity14`, `InvSerial14`, `Description_Look`, `Description_Look2`, `Description_Voice`, `status`) VALUES
(1, 1, 'Moroz', 0, 0, 0, 0, 0, 279, 0, 0, 3, 12274632, 0, 0, 2, 1, 3, 139, -100, 2, 313.235, 0, 0, 0, 0, 0, 0, 0, 0, 21, 1, 0, 0, 0, 0, 25, 46, 0, 22, 44, 0, 21, 49, 0, 8, 50, 0, 21, 48, 0, 18, 1, 4, 6, 1, 0, 5, 1, 0, 3, 378, 0, 2, 1, 0, 22, 1, 0, 21, 1, 0, 15, 1, 0, 25, 1, 0, 12, 1, 0, '6''5"', 'Looks gay', 'high-pitched voice', 0),
(2, 2, 'gephaest', 1360677149, 0, 0, 0, 0, 90, 0, 0, 3, 12799995, 0, 0, 1, 1, 3, 217, -67, 1, 6.78472, 0, 0, 0, 20, 0, 21, 54, 0, 15, 5, 4, 0, 0, 0, 25, 50, 0, 22, 45, 0, 21, 50, 0, 8, 50, 0, 9, 46, 0, 21, 50, 0, 18, 1, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', '', 0),
(3, 3, 'Lux', 0, 0, 0, 0, 0, 0, 0, 0, 3, 12739595, 0, 0, 1, 1, 3, 1512, -784, 78, 225.113, 0, 0, 0, 20, 0, 22, 45, 0, 15, 5, 4, 0, 0, 0, 25, 50, 0, 22, 45, 0, 21, 50, 0, 8, 50, 0, 9, 46, 0, 21, 50, 0, 18, 1, 4, 21, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', '', 0),
(4, 4, 'Hammster', 0, 0, 0, 0, 0, 0, 0, 0, 3, 12799995, 0, 0, 1, 1, 3, 217, -67, 1, 6.78472, 0, 0, 0, 20, 0, 21, 54, 0, 15, 5, 4, 0, 0, 0, 25, 50, 0, 22, 45, 0, 21, 50, 0, 8, 50, 0, 9, 46, 0, 21, 50, 0, 18, 1, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', '', 0),
(5, 5, 'Jester', 0, 0, 0, 0, 0, 27, 0, 0, 3, 12799995, 0, 0, 1, 1, 3, 123, -139, 2, 271.5, 0, 0, 25, 20, 0, 15, 1, 4, 21, 1, 0, 0, 0, 0, 22, 44, 0, 21, 48, 0, 8, 48, 0, 9, 44, 0, 21, 48, 0, 7, 4, 0, 21, 1, 0, 22, 1, 0, 21, 1, 0, 25, 1, 0, 21, 1, 0, 25, 1, 0, 25, 1, 0, 21, 1, 0, 18, 1, 4, '', '', '', 1),
(6, 6, 'TourP', 0, 0, 0, 0, 0, 0, 0, 0, 3, 12799995, 0, 0, 1, 1, 3, 217, -67, 1, 6.78472, 0, 0, 0, 20, 0, 21, 54, 0, 15, 5, 4, 0, 0, 0, 25, 50, 0, 22, 45, 0, 21, 50, 0, 8, 50, 0, 9, 46, 0, 21, 50, 0, 18, 1, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', '', 2),
(7, 7, 'Angrenost', 0, 0, 0, 0, 0, 0, 0, 0, 3, 12799995, 0, 0, 1, 1, 3, 217, -67, 1, 6.78472, 0, 0, 0, 20, 0, 21, 54, 0, 15, 5, 4, 0, 0, 0, 25, 50, 0, 22, 45, 0, 21, 50, 0, 8, 50, 0, 9, 46, 0, 21, 50, 0, 18, 1, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', '', 2),
(8, 8, 'Giper', 0, 0, 0, 0, 0, 0, 0, 0, 3, 12799995, 0, 0, 1, 1, 3, 217, -67, 1, 6.78472, 0, 0, 0, 20, 0, 21, 54, 0, 15, 5, 4, 0, 0, 0, 25, 50, 0, 22, 45, 0, 21, 50, 0, 8, 50, 0, 9, 46, 0, 21, 50, 0, 18, 1, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', '', 3),
(9, 9, 'Bob', 0, 0, 0, 0, 0, 0, 0, 0, 3, 12799995, 0, 0, 1, 1, 3, 217, -67, 1, 6.78472, 0, 0, 0, 20, 0, 21, 54, 0, 15, 5, 4, 0, 0, 0, 25, 50, 0, 22, 45, 0, 21, 50, 0, 8, 50, 0, 9, 46, 0, 21, 50, 0, 18, 1, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', '', 2),
(10, 10, 'Gary', 0, 0, 0, 0, 0, 22, 0, 0, 3, 12589995, 0, 0, 1, 1, 3, -54, 15, 3, 85.3502, 0, 0, 25, 20, 0, 15, 1, 4, 21, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `data_vehicles`
--

CREATE TABLE IF NOT EXISTS `data_vehicles` (
  `model` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  `type` enum('Car','SUV','Truck','Bike','Motorbike','Boat','Aerial','Train') NOT NULL,
  `tank` int(11) NOT NULL COMMENT 'Gas tank capacity',
  `comsumption` float NOT NULL COMMENT 'Fuel consumption per km',
  `wheels` int(11) NOT NULL COMMENT 'Number of wheels',
  `weight` int(11) NOT NULL COMMENT 'Vehicle''s weight',
  `trunkSize` int(11) NOT NULL COMMENT 'How many items vehicle can hold',
  `price` int(11) NOT NULL,
  PRIMARY KEY (`model`),
  KEY `model` (`model`),
  KEY `model_2` (`model`),
  KEY `model_3` (`model`)
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;

--
-- Dumping data for table `data_vehicles`
--

INSERT INTO `data_vehicles` (`model`, `name`, `type`, `tank`, `comsumption`, `wheels`, `weight`, `trunkSize`, `price`) VALUES
(400, 'Landstalker', 'Car', 55, 0.73, 4, 3000, 15, 50000),
(401, 'Bravura', 'Car', 45, 0.5, 4, 1900, 10, 25000),
(402, 'Buffalo', 'Car', 50, 0.63, 4, 2100, 4, 60000),
(403, 'Linerunner', 'Truck', 500, 3.15, 6, 7300, 30, 100000),
(404, 'Pereniel', 'Car', 30, 0.41, 4, 2300, 20, 15000),
(405, 'Sentinel', 'Car', 40, 0.55, 4, 2200, 10, 35000),
(406, 'Dumper', 'Truck', 300, 2.35, 4, 21000, 10, 0),
(407, 'Firetruck', 'Truck', 250, 1.2, 4, 9000, 10, 100000),
(408, 'Trashmaster', 'Truck', 200, 1.13, 6, 8400, 10, 55000),
(409, 'Stretch', 'Car', 50, 0.74, 4, 3500, 10, 65000),
(410, 'Manana', 'Car', 35, 0.34, 4, 1900, 5, 10000),
(411, 'Infernus', 'Car', 60, 0.75, 4, 2400, 10, 0),
(412, 'Voodoo', 'Car', 43, 0.54, 4, 2300, 10, 20000),
(413, 'Pony', 'Car', 60, 0.75, 4, 3600, 30, 60000),
(414, 'Mule', 'Car', 63, 0.76, 4, 3800, 40, 75000),
(415, 'Cheetah', 'Car', 55, 0.73, 4, 2200, 10, 0),
(416, 'Ambulance', 'Car', 59, 0.76, 4, 3800, 20, 100000),
(417, 'Leviathan', 'Aerial', 1500, 3.11, 2, 15000, 10, 500000),
(418, 'Moonbeam', 'Car', 50, 0.73, 4, 3600, 20, 25000),
(419, 'Esperanto', 'Car', 35, 0.54, 4, 2000, 10, 15000),
(420, 'Taxi', 'Car', 40, 0.55, 4, 2100, 10, 30000),
(421, 'Washington', 'Car', 52, 0.6, 4, 2300, 10, 30000),
(422, 'Bobcat', 'Car', 40, 0.61, 4, 3500, 20, 25000),
(423, 'Mr Whoopee', 'Car', 55, 0.65, 4, 3800, 30, 65000),
(424, 'BF Injection', 'Car', 20, 0.3, 4, 2500, 10, 0),
(425, 'Hunter', 'Aerial', 2000, 3.5, 2, 14000, 10, 0),
(426, 'Premier', 'Car', 47, 0.59, 4, 2400, 10, 40000),
(427, 'Enforcer', 'Truck', 130, 0.83, 4, 6000, 20, 105000),
(428, 'Securicar', 'Truck', 110, 0.81, 4, 5400, 10, 0),
(429, 'Banshee', 'Car', 50, 0.58, 4, 2600, 10, 0),
(430, 'Predator', 'Boat', 190, 0.95, 0, 400, 10, 85000),
(431, 'Bus', 'Truck', 80, 0.82, 6, 9000, 20, 100000),
(432, 'Rhino', 'Truck', 500, 3.05, 6, 9500, 10, 0),
(433, 'Barracks', 'Truck', 150, 0.91, 6, 9000, 10, 0),
(434, 'Hotknife', 'Car', 42, 0.6, 4, 2900, 10, 0),
(435, 'Trailer', 'Truck', -1, 0, 4, 4000, 10, 0),
(436, 'Previon', 'Car', 40, 0.5, 4, 2100, 10, 20000),
(437, 'Coach', 'Truck', 75, 0.79, 6, 8900, 10, 100000),
(438, 'Cabbie', 'Car', 45, 0.56, 4, 3400, 10, 20000),
(439, 'Stallion', 'Car', 40, 0.51, 4, 1900, 10, 45000),
(440, 'Rumpo', 'Car', 60, 0.65, 4, 3700, 25, 50000),
(441, 'RC Bandit', 'Car', -1, 0, 4, 10, 10, 0),
(442, 'Romero', 'Car', 50, 0.6, 4, 4000, 20, 65000),
(443, 'Packer', 'Truck', 400, 2.89, 6, 7400, 10, 0),
(444, 'Monster', '', 63, 0.7, 4, 6400, 10, 0),
(445, 'Admiral', 'Car', 45, 0.57, 4, 2300, 10, 50000),
(446, 'Squalo', 'Boat', 200, 0.87, 0, 450, 10, 60000),
(447, 'Seasparrow', 'Aerial', 1000, 4.08, 0, 5000, 10, 0),
(448, 'Pizzaboy', 'Motorbike', 5, 0.02, 2, 80, 10, 10000),
(449, 'Tram', 'Train', -1, 0, 0, 7000, 10, 0),
(450, 'Trailer', 'Truck', -1, 0, 4, 4000, 10, 0),
(451, 'Turismo', 'Car', 65, 0.65, 4, 2900, 10, 0),
(452, 'Speeder', 'Boat', 190, 0.85, 0, 470, 10, 30000),
(453, 'Reefer', 'Boat', 200, 0.88, 0, 500, 10, 75000),
(454, 'Tropic', 'Boat', 220, 0.9, 0, 490, 40, 85000),
(455, 'Flatbed', 'Truck', 80, 0.7, 6, 8100, 10, 0),
(456, 'Yankee', 'Truck', 65, 0.63, 4, 7900, 40, 85000),
(457, 'Caddy', 'Car', 20, 0.22, 4, 400, 10, 10000),
(458, 'Solair', 'Car', 50, 0.55, 4, 3100, 15, 25000),
(459, 'Berkley''s RC Van', 'Car', 60, 0.65, 4, 3800, 10, 0),
(460, 'Skimmer', 'Aerial', 1000, 4.1, 0, 8000, 10, 0),
(461, 'PCJ-600', 'Motorbike', 10, 0.1, 2, 350, 2, 25000),
(462, 'Faggio', 'Motorbike', 5, 0.02, 2, 80, 2, 10000),
(463, 'Freeway', 'Motorbike', 10, 0.09, 2, 390, 2, 15000),
(464, 'RC Baron', 'Aerial', -1, 0, 2, 10, 10, 0),
(465, 'RC Raider', 'Aerial', -1, 0, 0, 10, 10, 0),
(466, 'Glendale', 'Car', 45, 0.56, 4, 2900, 10, 25000),
(467, 'Oceanic', 'Car', 45, 0.56, 4, 2900, 10, 25000),
(468, 'Sanchez', 'Motorbike', 5, 0.05, 2, 90, 2, 10000),
(469, 'Sparrow', 'Aerial', 900, 3.45, 0, 5000, 10, 0),
(470, 'Patriot', '', 60, 0.63, 4, 3400, 20, 75000),
(471, 'Quad', 'Motorbike', 15, 0.15, 4, 110, 2, 15000),
(472, 'Coastguard', 'Boat', 150, 0.8, 0, 400, 10, 0),
(473, 'Dinghy', 'Boat', 140, 0.78, 0, 200, 10, 10000),
(474, 'Hermes', 'Car', 45, 0.56, 4, 2900, 10, 20000),
(475, 'Sabre', 'Car', 40, 0.5, 4, 2200, 10, 45000),
(476, 'Rustler', 'Aerial', 1700, 5, 3, 10000, 10, 0),
(477, 'ZR3 50', 'Car', 60, 0.6, 4, 2800, 8, 45000),
(478, 'Walton', 'Car', 45, 0.56, 4, 2700, 20, 15000),
(479, 'Regina', 'Car', 45, 0.59, 4, 3200, 20, 20000),
(480, 'Comet', 'Car', 45, 0.58, 4, 2500, 10, 60000),
(481, 'BMX', 'Bike', -1, 0, 2, 10, 0, 5000),
(482, 'Burrito', 'Car', 65, 0.65, 4, 3500, 30, 50000),
(483, 'Camper', 'Car', 60, 0.64, 4, 3400, 20, 20000),
(484, 'Marquis', 'Boat', 200, 0.94, 0, 1300, 10, 75000),
(485, 'Baggage', 'Truck', 25, 0.25, 4, 800, 10, 0),
(486, 'Dozer', 'Truck', 150, 1.9, 4, 8000, 10, 0),
(487, 'Maverick', 'Aerial', 2000, 5.3, 0, 4500, 10, 500000),
(488, 'News Chopper', 'Aerial', 2000, 5.3, 0, 4500, 10, 500000),
(489, 'Rancher', '', 50, 0.61, 4, 3800, 20, 35000),
(490, 'FBI Rancher', '', 55, 0.61, 4, 3800, 20, 60000),
(491, 'Virgo', 'Car', 50, 0.59, 4, 3000, 10, 20000),
(492, 'Greenwood', 'Car', 45, 0.57, 4, 2900, 10, 20000),
(493, 'Jetmax', 'Boat', 150, 0.88, 0, 1100, 10, 0),
(494, 'Hotring', 'Car', 55, 0.59, 4, 3000, 10, 0),
(495, 'Sandking', 'Car', 65, 0.65, 4, 3400, 10, 0),
(496, 'Blista Compact', 'Car', 40, 0.5, 4, 2900, 8, 25000),
(497, 'Police Maverick', 'Aerial', 2200, 5.54, 0, 4500, 10, 500000),
(498, 'Boxville', 'Car', 65, 0.64, 4, 3800, 10, 65000),
(499, 'Benson', 'Car', 60, 0.6, 4, 3700, 10, 60000),
(500, 'Mesa', 'Car', 55, 0.59, 4, 3500, 10, 25000),
(501, 'RC Goblin', 'Aerial', -1, 0, 0, 10, 10, 0),
(502, 'Hotring A', 'Car', 50, 0.6, 4, 3100, 10, 0),
(503, 'Hotring B', 'Car', 50, 0.6, 4, 3100, 10, 0),
(504, 'Bloodring Banger', 'Car', 40, 0.55, 4, 3000, 10, 0),
(505, 'Rancher', '', 60, 0.59, 4, 3500, 10, 25000),
(506, 'Super GT', 'Car', 65, 0.61, 4, 2900, 10, 0),
(507, 'Elegant', 'Car', 50, 0.58, 4, 2900, 10, 40000),
(508, 'Journey', 'Car', 80, 0.77, 4, 4100, 20, 60000),
(509, 'Bike', 'Bike', -1, 0, 2, 10, 10, 2500),
(510, 'Mountain Bike', 'Bike', -1, 0, 2, 10, 10, 6000),
(511, 'Beagle', 'Aerial', 1300, 3.13, 3, 10000, 10, 0),
(512, 'Cropdust', 'Aerial', 1150, 3.06, 3, 9000, 10, 0),
(513, 'Stunt', 'Aerial', 1100, 3, 3, 12000, 10, 0),
(514, 'Tanker', 'Truck', 500, 3.13, 6, 9000, 10, 0),
(515, 'RoadTrain', 'Truck', 550, 3.29, 6, 10000, 40, 125000),
(516, 'Nebula', 'Car', 44, 0.52, 4, 2400, 10, 20000),
(517, 'Majestic', 'Car', 42, 0.51, 4, 2200, 10, 30000),
(518, 'Buccaneer', 'Car', 41, 0.53, 4, 2300, 10, 30000),
(519, 'Shamal', 'Aerial', 2000, 3.21, 3, 20000, 10, 2500000),
(520, 'Hydra', 'Aerial', 3000, 4.55, 3, 90000, 10, 0),
(521, 'FCR-900', 'Motorbike', 8, 0.04, 2, 400, 2, 15000),
(522, 'NRG-500', 'Motorbike', 9, 0.05, 2, 400, 2, 0),
(523, 'HPV1000', 'Motorbike', 7, 0.03, 2, 380, 2, 20000),
(524, 'Cement Truck', 'Truck', 210, 1.15, 6, 10000, 10, 0),
(525, 'Tow Truck', '', 45, 0.59, 4, 4000, 20, 35000),
(526, 'Fortune', 'Car', 40, 0.53, 4, 1900, 10, 30000),
(527, 'Cadrona', 'Car', 40, 0.51, 4, 1900, 4, 5000),
(528, 'FBI Truck', 'Car', 60, 0.62, 4, 3900, 8, 75000),
(529, 'Willard', 'Car', 45, 0.51, 4, 2400, 10, 40000),
(530, 'Forklift', 'Car', 10, 0.09, 4, 1200, 10, 0),
(531, 'Tractor', 'Truck', 50, 0.6, 4, 3500, 10, 0),
(532, 'Combine', 'Truck', 250, 1.3, 4, 9000, 10, 0),
(533, 'Feltzer', 'Car', 46, 0.55, 4, 2400, 10, 70000),
(534, 'Remington', 'Car', 45, 0.55, 4, 2800, 10, 30000),
(535, 'Slamvan', 'Car', 45, 0.54, 4, 3000, 15, 50000),
(536, 'Blade', 'Car', 41, 0.5, 4, 2500, 10, 25000),
(537, 'Freight', 'Train', -1, 0, 8, 11000, 10, 0),
(538, 'Streak', 'Train', -1, 0, 8, 11000, 10, 0),
(539, 'Vortex', 'Car', 30, 0.41, 0, 1500, 10, 0),
(540, 'Vincent', 'Car', 44, 0.53, 4, 2900, 10, 60000),
(541, 'Bullet', 'Car', 60, 0.68, 4, 3000, 10, 0),
(542, 'Clover', 'Car', 40, 0.49, 4, 2100, 10, 20000),
(543, 'Sadler', 'Car', 40, 0.49, 4, 3000, 15, 20000),
(544, 'Firetruck', 'Truck', 210, 1.18, 4, 8000, 10, 100000),
(545, 'Hustler', 'Car', 50, 0.57, 4, 3200, 10, 60000),
(546, 'Intruder', 'Car', 40, 0.52, 4, 2600, 10, 45000),
(547, 'Primo', 'Car', 42, 0.51, 4, 2500, 10, 40000),
(548, 'Cargobob', 'Aerial', 2000, 3.45, 3, 13000, 10, 0),
(549, 'Tampa', 'Car', 45, 0.53, 4, 3000, 10, 25000),
(550, 'Sunrise', 'Car', 41, 0.5, 4, 2900, 10, 60000),
(551, 'Merit', 'Car', 42, 0.49, 4, 2800, 10, 50000),
(552, 'Utility', 'Car', 55, 0.61, 4, 3400, 10, 75000),
(553, 'Nevada', 'Aerial', 1500, 3.02, 3, 50000, 10, 0),
(554, 'Yosemite', 'Car', 60, 0.61, 4, 3500, 20, 45000),
(555, 'Windsor', 'Car', 47, 0.54, 4, 2900, 10, 55000),
(556, 'Monster A', 'SUV', 63, 0.7, 4, 4100, 10, 0),
(557, 'Monster B', 'SUV', 63, 0.7, 4, 4100, 10, 0),
(558, 'Uranus', 'Car', 53, 0.59, 4, 3000, 10, 45000),
(559, 'Jester', 'Car', 55, 0.61, 4, 2800, 10, 50000),
(560, 'Sultan', 'Car', 60, 0.65, 4, 3100, 8, 100000),
(561, 'Stratum', 'Car', 46, 0.51, 4, 3200, 15, 55000),
(562, 'Elegy', 'Car', 54, 0.6, 4, 2900, 10, 60000),
(563, 'Raindance', 'Aerial', 1700, 3.2, 3, 11000, 10, 0),
(564, 'RC Tiger', 'Car', 1, 0.01, 4, 10, 10, 0),
(565, 'Flash', 'Car', 48, 0.55, 4, 3200, 10, 50000),
(566, 'Tahoma', 'Car', 50, 0.54, 4, 2800, 10, 30000),
(567, 'Savanna', 'Car', 48, 0.5, 4, 2600, 10, 30000),
(568, 'Bandito', 'Car', 30, 0.41, 4, 2000, 10, 0),
(569, 'Freight', 'Car', -1, 0, 8, 8000, 10, 0),
(570, 'Trailer', 'Car', -1, 0, 8, 10000, 10, 0),
(571, 'Kart', 'Car', 4, 0.03, 4, 100, 10, 0),
(572, 'Mower', 'Car', 15, 0.1, 4, 150, 10, 0),
(573, 'Duneride', 'Truck', 340, 1.43, 4, 9000, 10, 0),
(574, 'Sweeper', 'Car', 23, 0.2, 4, 1100, 10, 0),
(575, 'Broadway', 'Car', 45, 5.6, 4, 3000, 10, 25000),
(576, 'Tornado', 'Car', 45, 5.63, 4, 2900, 10, 25000),
(577, 'AT-400', 'Aerial', 1700, 3.42, 3, 80000, 10, 0),
(578, 'DFT-30', 'Truck', 230, 1, 4, 7000, 10, 0),
(579, 'Huntley', 'SUV', 57, 0.6, 4, 4000, 15, 75000),
(580, 'Stafford', 'Car', 45, 0.51, 4, 3400, 10, 200000),
(581, 'BF-400', 'Motorbike', 8, 0.04, 2, 390, 2, 15000),
(582, 'Newsvan', 'Car', 59, 0.67, 4, 4200, 10, 60000),
(583, 'Tug', 'Car', 20, 0.25, 4, 1000, 10, 0),
(584, 'Trailer A', 'Car', -1, 0, 4, 3000, 10, 0),
(585, 'Emperor', 'Car', 40, 0.49, 4, 2400, 10, 30000),
(586, 'Wayfarer', 'Motorbike', 6, 0.03, 2, 400, 4, 20000),
(587, 'Euros', 'Car', 50, 0.6, 4, 2800, 8, 30000),
(588, 'Hotdog', 'Car', 60, 0.62, 4, 5400, 10, 55000),
(589, 'Club', 'Car', 45, 0.57, 4, 3300, 10, 25000),
(590, 'Trailer B', 'Car', -1, 0, 8, 6000, 10, 0),
(591, 'Trailer C', 'Car', -1, 0, 4, 2000, 10, 0),
(592, 'Andromada', 'Aerial', 2300, 3.55, 4, 100000, 10, 0),
(593, 'Dodo', 'Aerial', 1000, 3.01, 3, 8000, 10, 500000),
(594, 'RC Cam', 'Car', 1, 0.01, 0, 10, 10, 0),
(595, 'Launch', 'Boat', 150, 0.9, 0, 2000, 10, 0),
(596, 'Police Car', 'Car', 45, 0.53, 4, 3100, 8, 65000),
(597, 'Police Car', 'Car', 45, 0.53, 4, 3100, 8, 65000),
(598, 'Police Car', 'Car', 45, 0.53, 4, 3100, 8, 65000),
(599, 'Police Ranger', 'Truck', 54, 0.61, 4, 3700, 12, 55000),
(600, 'Picador', 'Car', 45, 0.51, 4, 2900, 10, 25000),
(601, 'S.W.A.T.', 'Truck', 60, 0.75, 4, 4500, 10, 110000),
(602, 'Alpha', 'Car', 57, 0.62, 4, 3000, 10, 50000),
(603, 'Phoenix', 'Car', 55, 0.6, 4, 3100, 10, 55000),
(604, 'Glendale', 'Car', 35, 0.49, 4, 2900, 10, 0),
(605, 'Sadler', 'Car', 35, 0.49, 4, 3000, 10, 0),
(606, 'L Trailer A', 'Car', -1, 0, 4, 120, 10, 0),
(607, 'L Trailer B', 'Car', -1, 0, 4, 100, 10, 0),
(608, 'Stair Trailer', 'Car', -1, 0, 4, 100, 10, 0),
(609, 'Boxville', 'Car', 62, 0.71, 4, 4200, 10, 75000),
(610, 'Farm Plow', 'Car', -1, 0, 2, 400, 10, 0),
(611, 'U Trailer', 'Car', -1, 0, 4, 500, 10, 0);

-- --------------------------------------------------------

--
-- Table structure for table `droppeditems`
--

CREATE TABLE IF NOT EXISTS `droppeditems` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ItemID` int(11) DEFAULT NULL,
  `ItemQuantity` int(11) DEFAULT NULL,
  `DropTime` int(11) DEFAULT NULL,
  `DroppedBy` int(11) DEFAULT NULL,
  `PosX` float DEFAULT NULL,
  `PosY` float DEFAULT NULL,
  `PosZ` float DEFAULT NULL,
  `VirtualWorld` int(11) DEFAULT NULL,
  `Interior` int(11) DEFAULT NULL,
  `Serial` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=cp1251 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `factions`
--

CREATE TABLE IF NOT EXISTS `factions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(32) DEFAULT NULL,
  `Owner` int(11) DEFAULT NULL,
  `Type` int(11) DEFAULT NULL,
  `Bank` int(11) NOT NULL DEFAULT '0',
  `PosX` float DEFAULT NULL,
  `PosY` float DEFAULT NULL,
  `PosZ` float DEFAULT NULL,
  `RankNames` varchar(254) DEFAULT NULL,
  `RankPaycheck` varchar(76) NOT NULL DEFAULT '',
  `RankAccess` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=cp1251 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `factions`
--

INSERT INTO `factions` (`id`, `Name`, `Owner`, `Type`, `Bank`, `PosX`, `PosY`, `PosZ`, `RankNames`, `RankPaycheck`, `RankAccess`) VALUES
(1, 'LSFD', 1, 3, 23123, 0, 0, 0, 'First-R;EMT-B;Nurse-N;Nurse-H;Nurse-S;FF-FR;EMT-P;EMT-H;EMT-S;EMS-B.C', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `log_kick`
--

CREATE TABLE IF NOT EXISTS `log_kick` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `PlayerID` int(11) NOT NULL,
  `AdminID` int(11) NOT NULL,
  `Reason` varchar(128) NOT NULL,
  `IP` varchar(64) NOT NULL,
  `Timestamp` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=cp1251 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `maps`
--

CREATE TABLE IF NOT EXISTS `maps` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group` varchar(24) NOT NULL,
  `model` int(11) NOT NULL,
  `PosX` float NOT NULL,
  `PosY` float NOT NULL,
  `PosZ` float NOT NULL,
  `RotX` float NOT NULL,
  `RotY` float NOT NULL,
  `RotZ` float NOT NULL,
  `Interior` int(11) NOT NULL,
  `VirtualWorld` int(11) NOT NULL,
  `AccessType` enum('Faction','Job') NOT NULL,
  `AccessValue` int(11) NOT NULL,
  `MovedPosX` float NOT NULL,
  `MovedPosY` float NOT NULL,
  `MovedPosZ` float NOT NULL,
  `MovedRotX` float NOT NULL,
  `MovedRotY` float NOT NULL,
  `MovedRotZ` float NOT NULL,
  `MoveRadius` float NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=cp1251 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `players`
--

CREATE TABLE IF NOT EXISTS `players` (
  `PlayerID` int(11) NOT NULL AUTO_INCREMENT,
  `Username` varchar(24) NOT NULL,
  `Email` varchar(128) NOT NULL,
  `Password` varchar(128) NOT NULL,
  `Registered` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `AdminLevel` int(11) NOT NULL DEFAULT '0',
  `AdmCode` varchar(128) NOT NULL DEFAULT '',
  PRIMARY KEY (`PlayerID`),
  UNIQUE KEY `Username` (`Username`)
) ENGINE=InnoDB  DEFAULT CHARSET=cp1251 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `players`
--

INSERT INTO `players` (`PlayerID`, `Username`, `Email`, `Password`, `Registered`, `AdminLevel`, `AdmCode`) VALUES
(1, 'Moroz', 'user@example.com', '344907e89b981caf221d05f597eb57a6af408f15f4dd7895bbd1b96a2938ec24a7dcf23acb94ece0b6d7b0640358bc56bdb448194b9305311aff038a834a079f', '2013-01-08 12:12:22', 10, '344907e89b981caf221d05f597eb57a6af408f15f4dd7895bbd1b96a2938ec24a7dcf23acb94ece0b6d7b0640358bc56bdb448194b9305311aff038a834a079f'),
(2, 'gephaest', 'user@example.com', '344907e89b981caf221d05f597eb57a6af408f15f4dd7895bbd1b96a2938ec24a7dcf23acb94ece0b6d7b0640358bc56bdb448194b9305311aff038a834a079f', '2013-01-08 12:12:22', 10, '344907e89b981caf221d05f597eb57a6af408f15f4dd7895bbd1b96a2938ec24a7dcf23acb94ece0b6d7b0640358bc56bdb448194b9305311aff038a834a079f'),
(3, 'Lux', 'user@example.com', '344907e89b981caf221d05f597eb57a6af408f15f4dd7895bbd1b96a2938ec24a7dcf23acb94ece0b6d7b0640358bc56bdb448194b9305311aff038a834a079f', '2013-01-08 12:12:22', 10, '344907e89b981caf221d05f597eb57a6af408f15f4dd7895bbd1b96a2938ec24a7dcf23acb94ece0b6d7b0640358bc56bdb448194b9305311aff038a834a079f'),
(4, 'Hammster', 'user@example.com', '344907e89b981caf221d05f597eb57a6af408f15f4dd7895bbd1b96a2938ec24a7dcf23acb94ece0b6d7b0640358bc56bdb448194b9305311aff038a834a079f', '2013-01-08 12:12:22', 10, '344907e89b981caf221d05f597eb57a6af408f15f4dd7895bbd1b96a2938ec24a7dcf23acb94ece0b6d7b0640358bc56bdb448194b9305311aff038a834a079f'),
(5, 'Jester', 'user@example.com', '344907e89b981caf221d05f597eb57a6af408f15f4dd7895bbd1b96a2938ec24a7dcf23acb94ece0b6d7b0640358bc56bdb448194b9305311aff038a834a079f', '2013-01-08 12:12:22', 10, '344907e89b981caf221d05f597eb57a6af408f15f4dd7895bbd1b96a2938ec24a7dcf23acb94ece0b6d7b0640358bc56bdb448194b9305311aff038a834a079f'),
(6, 'TourP', 'user@example.com', '344907e89b981caf221d05f597eb57a6af408f15f4dd7895bbd1b96a2938ec24a7dcf23acb94ece0b6d7b0640358bc56bdb448194b9305311aff038a834a079f', '2013-01-08 12:12:22', 10, '344907e89b981caf221d05f597eb57a6af408f15f4dd7895bbd1b96a2938ec24a7dcf23acb94ece0b6d7b0640358bc56bdb448194b9305311aff038a834a079f'),
(7, 'Angrenost', 'user@example.com', '344907e89b981caf221d05f597eb57a6af408f15f4dd7895bbd1b96a2938ec24a7dcf23acb94ece0b6d7b0640358bc56bdb448194b9305311aff038a834a079f', '2013-01-08 12:12:22', 10, '344907e89b981caf221d05f597eb57a6af408f15f4dd7895bbd1b96a2938ec24a7dcf23acb94ece0b6d7b0640358bc56bdb448194b9305311aff038a834a079f'),
(8, 'Giper', 'user@example.com', '344907e89b981caf221d05f597eb57a6af408f15f4dd7895bbd1b96a2938ec24a7dcf23acb94ece0b6d7b0640358bc56bdb448194b9305311aff038a834a079f', '2013-01-08 12:12:22', 10, '344907e89b981caf221d05f597eb57a6af408f15f4dd7895bbd1b96a2938ec24a7dcf23acb94ece0b6d7b0640358bc56bdb448194b9305311aff038a834a079f'),
(9, 'Bob', 'user@example.com', '344907e89b981caf221d05f597eb57a6af408f15f4dd7895bbd1b96a2938ec24a7dcf23acb94ece0b6d7b0640358bc56bdb448194b9305311aff038a834a079f', '2013-01-08 12:12:22', 10, '344907e89b981caf221d05f597eb57a6af408f15f4dd7895bbd1b96a2938ec24a7dcf23acb94ece0b6d7b0640358bc56bdb448194b9305311aff038a834a079f'),
(10, 'Gary', 'user@example.com', '344907e89b981caf221d05f597eb57a6af408f15f4dd7895bbd1b96a2938ec24a7dcf23acb94ece0b6d7b0640358bc56bdb448194b9305311aff038a834a079f', '2013-01-08 12:12:22', 10, '344907e89b981caf221d05f597eb57a6af408f15f4dd7895bbd1b96a2938ec24a7dcf23acb94ece0b6d7b0640358bc56bdb448194b9305311aff038a834a079f');

-- --------------------------------------------------------

--
-- Table structure for table `property_bizz`
--

CREATE TABLE IF NOT EXISTS `property_bizz` (
  `id` int(11) NOT NULL,
  `Prods` int(11) NOT NULL DEFAULT '0',
  `MaxProds` int(11) NOT NULL DEFAULT '100',
  `PriceLevel` int(11) NOT NULL DEFAULT '1',
  `Money` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_2` (`id`),
  KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;

-- --------------------------------------------------------

--
-- Table structure for table `property_furniture`
--

CREATE TABLE IF NOT EXISTS `property_furniture` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Property` int(11) NOT NULL,
  `Item` int(11) NOT NULL COMMENT 'Inventory item representing the object',
  `PosX` float NOT NULL,
  `PosY` float NOT NULL,
  `PosZ` float NOT NULL,
  `RotX` float NOT NULL,
  `RotY` float NOT NULL,
  `RotZ` float NOT NULL,
  `Interior` int(11) NOT NULL,
  `VirtualWorld` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=cp1251 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `property_info`
--

CREATE TABLE IF NOT EXISTS `property_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Type` int(2) NOT NULL DEFAULT '0',
  `OwnerID` int(11) NOT NULL COMMENT 'ID владельца',
  `Enter` varchar(60) NOT NULL COMMENT 'X,Y,Z,A',
  `Interior` int(11) NOT NULL COMMENT 'Интерьер',
  `Description` varchar(30) NOT NULL DEFAULT 'Justa house' COMMENT 'Описание',
  `Alarm` int(2) NOT NULL DEFAULT '0' COMMENT 'Уровень сигнализации',
  `Alarmed` int(1) NOT NULL DEFAULT '0',
  `Lock` int(2) NOT NULL DEFAULT '1' COMMENT 'Уровень замка',
  `Locked` int(1) NOT NULL DEFAULT '0' COMMENT 'Открыт ли замок',
  `Status` int(1) NOT NULL DEFAULT '0' COMMENT 'Статус',
  `Cost` int(11) NOT NULL DEFAULT '1' COMMENT 'Цена покупки',
  PRIMARY KEY (`id`),
  KEY `OwnerID` (`OwnerID`),
  KEY `OwnerID_2` (`OwnerID`),
  KEY `OwnerID_3` (`OwnerID`)
) ENGINE=InnoDB  DEFAULT CHARSET=cp1251 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `property_info`
--

INSERT INTO `property_info` (`id`, `Type`, `OwnerID`, `Enter`, `Interior`, `Description`, `Alarm`, `Alarmed`, `Lock`, `Locked`, `Status`, `Cost`) VALUES
(1, 0, 0, '2495.327880,-1690.736816,14.765625,181.012603', 2, 'New property', 0, 0, 1, 0, 0, 100),
(2, 0, 0, '2514.304443,-1691.546142,14.046038,232.086349', 3, 'New property', 0, 0, 1, 0, 0, 100),
(3, 0, 0, '2523.004882,-1679.344116,15.496999,265.613372', 4, 'New property', 0, 0, 1, 0, 0, 100),
(4, 0, 0, '2524.602294,-1658.650512,15.824020,274.699981', 5, 'New property', 0, 0, 1, 0, 0, 100),
(5, 0, 0, '2513.420654,-1650.423095,14.355666,317.313598', 6, 'New property', 0, 0, 1, 0, 0, 100),
(6, 0, 9, '2498.497558,-1642.252807,14.113082,2.120618', 7, 'New property', 0, 0, 1, 0, 1, 100),
(7, 0, 9, '2486.507080,-1645.292846,14.077178,359.614379', 8, 'New property', 0, 0, 1, 0, 1, 100);

-- --------------------------------------------------------

--
-- Table structure for table `radiostations`
--

CREATE TABLE IF NOT EXISTS `radiostations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(64) NOT NULL,
  `URL` varchar(256) NOT NULL,
  `Genre` int(2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=cp1251 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `storages`
--

CREATE TABLE IF NOT EXISTS `storages` (
  `storage` int(11) NOT NULL,
  `type` int(11) NOT NULL COMMENT '0 - inv storage, 1 - car',
  `item` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `serial` int(11) NOT NULL,
  `time` int(11) NOT NULL,
  `droppedBy` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;

-- --------------------------------------------------------

--
-- Table structure for table `vehicles`
--

CREATE TABLE IF NOT EXISTS `vehicles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Owner` int(11) DEFAULT NULL,
  `Plate` varchar(32) CHARACTER SET latin1 DEFAULT ' ',
  `Model` int(11) DEFAULT NULL,
  `Color1` int(11) DEFAULT NULL,
  `Color2` int(11) DEFAULT NULL,
  `PaintJob` int(11) DEFAULT '0',
  `Components` varchar(100) DEFAULT NULL,
  `Health` float DEFAULT '1000',
  `DamageStatus` varchar(32) DEFAULT '0 0 0 0',
  `PosX` float DEFAULT NULL,
  `PosY` float DEFAULT NULL,
  `PosZ` float DEFAULT NULL,
  `RotZ` float DEFAULT NULL,
  `Interior` int(11) DEFAULT NULL,
  `VirtualWorld` int(11) DEFAULT NULL,
  `CreatedTime` int(11) DEFAULT NULL,
  `Flags` varchar(32) DEFAULT '0 0 0 0 0 0',
  `Lock` int(11) NOT NULL DEFAULT '0',
  `Alarm` int(11) NOT NULL DEFAULT '0',
  `Immobilizer` int(11) NOT NULL DEFAULT '0',
  `Faction` int(11) NOT NULL DEFAULT '0',
  `Insurance` int(11) NOT NULL DEFAULT '2',
  `Deaths` int(11) NOT NULL DEFAULT '0',
  `Disabled` int(11) NOT NULL DEFAULT '0',
  `InvItems` varchar(170) NOT NULL DEFAULT '',
  `InvQuantity` varchar(250) NOT NULL DEFAULT '',
  `InvSerial` varchar(370) NOT NULL DEFAULT '',
  `Fuel` float DEFAULT NULL,
  `Mileage` float DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=cp1251 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `vehicles`
--

INSERT INTO `vehicles` (`id`, `Owner`, `Plate`, `Model`, `Color1`, `Color2`, `PaintJob`, `Components`, `Health`, `DamageStatus`, `PosX`, `PosY`, `PosZ`, `RotZ`, `Interior`, `VirtualWorld`, `CreatedTime`, `Flags`, `Lock`, `Alarm`, `Immobilizer`, `Faction`, `Insurance`, `Deaths`, `Disabled`, `InvItems`, `InvQuantity`, `InvSerial`) VALUES
(1, 28, '1488', 460, 2, 3, 0, NULL, 1000, '0 0 0 0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0 0 0 0 0 0', 0, 0, 0, 0, 2, 0, 0, '', '', ''),
(2, 28, '1255', 462, 3, 4, 0, NULL, 1000, '0 0 0 0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0 0 0 0 0 0', 0, 0, 0, 0, 2, 0, 0, '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `whitelist`
--

CREATE TABLE IF NOT EXISTS `whitelist` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `PlayerID` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `PlayerID` (`PlayerID`)
) ENGINE=InnoDB DEFAULT CHARSET=cp1251 AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
