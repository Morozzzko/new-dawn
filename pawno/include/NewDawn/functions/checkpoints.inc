/**
 * Functions for working with checkpoints
 *
 * @author Moroz <moroz@malefics.com>
 * @see __SeePlayerCheckpointEx
 */


#if defined checkpoint_handler_included
	#endinput
#endif
#define checkpoint_handler_included


 
enum cpInfo {
	cpID,
	Float:cpX,
	Float:cpY,
	Float:cpZ,
	Float:cpSize,
	cpCallback[32]
};

new CheckpointInfo[MAX_PLAYERS][cpInfo];

/**
 * Sets a checkpoint.
 * Arguments are the same except for two:
 * @param int checkpointid - custom checkpoint id
 * @param string callback - a function that will be called when player steps on the CP
 */

stock SetPlayerCheckpointEx(playerid, Float:x, Float:y, Float:z, Float:size, checkpointid = 0, callback[] = "") {
	CheckpointInfo[playerid][cpID] = checkpointid;
	CheckpointInfo[playerid][cpX] = x;
	CheckpointInfo[playerid][cpY] = y;
	CheckpointInfo[playerid][cpZ] = z;
	CheckpointInfo[playerid][cpSize] = size;
	strmid(CheckpointInfo[playerid][cpCallback], callback, 0, 32);
	return SetPlayerCheckpoint(playerid, x, y, z, size);
}
#define SetPlayerCheckpoint SetPlayerCheckpointEx

stock DisablePlayerCheckpointEx(playerid) {
	CheckpointInfo[playerid][cpID] = 0;
	CheckpointInfo[playerid][cpX] = 0.0;
	CheckpointInfo[playerid][cpY] = 0.0;
	CheckpointInfo[playerid][cpZ] = 0.0;
	CheckpointInfo[playerid][cpSize] = 0.0;
	return DisablePlayerCheckpoint(playerid);
}
#define DisablePlayerCheckpoint DisablePlayerCheckpointEx

hook OnPlayerEnterCheckpoint(playerid) {
	if(CheckpointInfo[playerid][cpID] != 0) {
		if(IsPlayerInRangeOfPoint(
			playerid, 
			CheckpointInfo[playerid][cpSize], 
			CheckpointInfo[playerid][cpX], 
			CheckpointInfo[playerid][cpY],
			CheckpointInfo[playerid][cpZ])
		) {
			if(strlen(CheckpointInfo[playerid][cpCallback]) != 0) {
				CallLocalFunction(CheckpointInfo[playerid][cpCallback], "dd", playerid, CheckpointInfo[playerid][cpID]);
				return 1;
			}
			else {
				GameTextForPlayer(playerid, "~p~YOU HAVE FOUND IT!", 1000, 4);
				DisablePlayerCheckpoint(playerid);
				return 1;
			}
		}
	}
	return 1;
}