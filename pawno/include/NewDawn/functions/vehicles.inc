/**
 * ������� ��� ������ � �����������
 * @author Moroz <moroz@malefics.com>
 * @author Jonathan_Rosewood <jonathan-rosewood@yandex.ru>
 *
 * �������:
 * @see GetVehicleName(vehiclemodel)
 * @see MoveVehicle(vehicleid, Float:PosX, Float:PosY, Float:PosZ, Float:Angle=0.0, Interior=0, VirtualWorld=0)
 * @see GetVehicleSpeed
 *
 */
 
 
 
/**
 * Returns vehicle speed in kmph
 *
 * @author #Hacker
 * @author Moroz
 * @param int vehicle id
 * @returns rounded vehicle speed
 */
stock Float:GetVehicleSpeed(vehicleid, bool:round = true) 
{
		new
	  		Float:Pos[3];
  		GetVehicleVelocity(vehicleid,Pos[0],Pos[1],Pos[2]);
		if(round) {
			return Float:floatround(200.0*floatsqroot(Pos[0]*Pos[0]+Pos[1]*Pos[1]+Pos[2]*Pos[2]));
		}
		return 200.0*floatsqroot(Pos[0]*Pos[0]+Pos[1]*Pos[1]+Pos[2]*Pos[2]);
}

 


stock AddStaticVehicle_Iter(modelid, Float:spawn_x, Float:spawn_y, Float:spawn_z, Float:z_angle, color1, color2) {
	new veh = AddStaticVehicle(modelid, spawn_x, spawn_y, spawn_z, z_angle, color1, color2);
	Iter_Add(Vehicle, veh);
	return veh;
}

#define AddStaticVehicle AddStaticVehicle_Iter

stock AddStaticVehicleEx_Iter(modelid, Float:spawn_x, Float:spawn_y, Float:spawn_z, Float:z_angle, color1, color2, respawn_delay) {
	new veh = AddStaticVehicleEx(modelid, spawn_x, spawn_y, spawn_z, z_angle, color1, color2, respawn_delay);
	Iter_Add(Vehicle, veh);
	return veh;
}

#define AddStaticVehicleEx AddStaticVehicleEx_Iter

stock CreateVehicle_Iter(modelid, Float:spawn_x, Float:spawn_y, Float:spawn_z, Float:z_angle, color1, color2, respawn_delay) {
	new veh = CreateVehicle(modelid, spawn_x, spawn_y, spawn_z, z_angle, color1, color2, respawn_delay);
	Iter_Add(Vehicle, veh);
	return veh;
}

#define CreateVehicle CreateVehicle_Iter

stock DestroyVehicle_Iter(vehicleid) {
	DestroyVehicle(vehicleid);
	if(Vehicles[vehicleid][vOwnerID] != INVALID_PLAYER_ID) {
		PVeh[Vehicles[vehicleid][vOwnerID]][GetSpawnedVehicleSlot(Vehicles[vehicleid][vOwnerID])][pvCar] = 0;
		DeletePVar(Vehicles[vehicleid][vOwnerID], "SpawnedVehicleSlot");
	}
	Iter_Remove(Vehicle, vehicleid);
}
#define DestroyVehicle DestroyVehicle_Iter



#if defined vehicles_included

stock LinkVehicleToInteriorEx(vehicleid, interiorid) {
	Vehicles[vehicleid][vInt] = interiorid;
	return LinkVehicleToInterior(vehicleid, interiorid);
}
#define LinkVehicleToInterior LinkVehicleToInteriorEx

stock GetVehicleInventory(vehicleid) {
	if(vehicleid < 0 || vehicleid > MAX_VEHICLES) {
		return 0;
	}
	return Vehicles[vehicleid][vInt];
}

/**
 * Returns vehicle name
 * 
 * @param int model ID (400-611)
 * @return string containing vehicle name
 */

stock GetVehicleName(vehiclemodel) {
	if(vehiclemodel < 400 || vehiclemodel > 611) 
		return 0;
	return VehInfo[vehiclemodel-400][V_C_NAME];
}

#endif

/**
 * ���������� ��������� � ��������� ����������, �������� � ����������� ���
 *
 * @param int id ����������
 * @param float ������� �� ��� x
 * @param float ������� �� ��� y
 * @param float ������� �� ��� z
 * @param float ���� �������� �� ��� z
 * @param int ����� ���������
 * @param int ����� ������������ ����
 *
 */

stock MoveVehicle(vehicleid, Float:PosX, Float:PosY, Float:PosZ, Float:Angle=0.0, Interior=0, VirtualWorld=0) {

	SetVehiclePos(vehicleid, PosX, PosY, PosZ);
	SetVehicleZAngle(vehicleid, Angle);
	LinkVehicleToInterior(vehicleid, Interior);
	SetVehicleVirtualWorld(vehicleid, VirtualWorld);
	return true;
}
