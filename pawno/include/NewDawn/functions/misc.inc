/* A litle MySQL fix */

#undef mysql_query
stock mysql_query(query[], connectionHandle=1, bool:cached=false) {
	return mysql_function_query(connectionHandle,query,cached,"OnQueryFinish","siii",query,-1,-1,1);
}
	
#undef mysql_query_callback
stock mysql_query_callback(index, query[], callback[], extraid = (-1), connectionHandle = 1, bool:cached = false) {
	return mysql_function_query(connectionHandle, query, cached, callback, "siii", query, index, extraid, connectionHandle);
}

stock Float:GetDistanceBetweenPoints3D(Float:X, Float:Y, Float:Z, Float:x, Float:y, Float:z) {
	return floatsqroot(X*X-x*x + Y*Y-y*y + Z*Z-z*z);
}


// Since sscanf does not work with arrays of strings...
stock split(const strsrc[], strdest[][], delimiter)
{
    new i, li;
    new aNum;
    new len;
    while(i <= strlen(strsrc))
    {
        if(strsrc[i] == delimiter || i == strlen(strsrc))
        {
            len = strmid(strdest[aNum], strsrc, li, i, 128);
            strdest[aNum][len] = 0;
            li = i+1;
            aNum++;
        }
        i++;
    }
    return 1;
}

/**
 * Splits a string into two lines
 * Make sure destination is an array of two strings
 * If the string's length not greater than 64, it won't split
 */
 
stock strsplit(source[], dest[][]) {
	new length = strlen(source), idx=63;
	if(length <= 64) {
		strmid(dest[0], source, 0, length, OUTPUT);
		strmid(dest[1], "", 0, length, OUTPUT);
		return;
	}
	while(source[idx] != ' '  && source[idx] != EOS) {
		idx++;
	}
	if(idx > 80)
		idx = 64;   
	strmid(dest[0], source, 0, idx, OUTPUT);
	// removing spaces
	while(source[idx] == ' '  && source[idx] != EOS) {
		idx++;
	}
	strmid(dest[1], source, idx, length, OUTPUT);
	return;
}

/**
 * Formats message and splits it into two strings if needed
 */

stock FormatMessage(destination[][], start[], text[], ending[]) {
	new buffer[2][OUTPUT];
	strsplit(text, buffer);
	if(strlen(buffer[1]) > 0) {
		format(destination[0], OUTPUT, "%s %s ... %s", start, buffer[0], ending);
		format(destination[1], OUTPUT, "%s ... %s %s", start, buffer[1], ending);
	}
	else {
		format(destination[0], OUTPUT, "%s %s %s", start, buffer[0], ending);
		format(destination[1], OUTPUT, "");
	}
}

/** 
 * Tells whether the string is integer or not 
 * @return 1 if the string is integer, 0 otherwise
 */
stock is_integer(const string[], len = sizeof(string)) {
	for(new i=0;i<len;i++) {
		if(string[i] < '0' || string[i] > '9') {
			return 0;
		}
	}
	return 1;
}

/** 
 * Tells whether the string is a number (integer or with a floating point) or not 
 * @return 1 if the string is numeric, 0 otherwise
 */
stock is_numeric(const string[], len = sizeof(string)) {
	for(new i=0;i<len;i++) {
		if((string[i] < '0' || string[i] > '9') && string[i] != '.' && string[i] != ',') {
			return 0;
		}
	}
	return 1;
}
