/**
 * offers.inc
 * @author gim <myvrenik@gmail.com>
 * @author Moroz <moroz@malefics.com>
 * 
 * Functions:
 * @see CreateOffer(playerid, targetid, string:text1[], string:text2[], string:callback[])
 * @see CreateConfirmationDialog(playerid, string:content[], string:callback[], extraid=0)
 * @see CreateConfirmationDialogEx(playerid, string:content[], string:callback[], oktext[], notext[], extraid=0)
 * @see ReturnUser(playerid, string:content[], string:callback[])
 * 
 */
#if defined __offers_inc_included
    #endinput
#endif
#define __offers_inc_included

/**
 * Show confirmation dialog to player and proceed the result in to the callback
 * 
 * @param int playerid:
 * @param string content: The text to show in the dialog window
 * @param string callback: Callback name where proceed result to
 * @param int extraid: Extra variable
 */
stock CreateConfirmationDialog(playerid, string:content[], string:callback[], extraid=0) {
    Dialog_ShowCallback(playerid, using callback Dialog_ShowConfirmationCB, DIALOG_STYLE_MSGBOX, "Confirm action", content, "Yes", "No");
    SetPVarString(playerid, "DialogCallback", callback);
    SetPVarInt(playerid, "_confirmExtraid", extraid);
    return 1;
}
/**
 * Show confirmation dialog to player and proceed the result in to the callback.
 * Allows to set a non-constant button text label.
 * 
 * @param int playerid:
 * @param string content: The text to show in the dialog window
 * @param string callback: Callback name where proceed result to
 * @param string oktext: Confirm button text label
 * @param string notext: Cancel button text label
 * @param int extraid: Extra variable
 */
stock CreateConfirmationDialogEx(playerid, string:content[], string:callback[], string:oktext[], string:notext[], extraid=0) {
    Dialog_ShowCallback(playerid, using callback Dialog_ShowConfirmationCB, DIALOG_STYLE_MSGBOX, "Confirm action", content, oktext, notext);
    SetPVarString(playerid, "DialogCallback", callback);
    SetPVarInt(playerid, "_confirmExtraid", extraid);
    return 1;
}
forward Dialog_ShowConfirmationCB(playerid, dialogid, response, listitem, string:inputtext[]);
public Dialog_ShowConfirmationCB(playerid, dialogid, response, listitem, string:inputtext[]) {
    new callback[32];
    GetPVarString(playerid, "DialogCallback", callback, sizeof(callback));
    CallLocalFunction(callback, "iii", playerid, response, GetPVarInt(playerid, "_confirmExtraid"));
    return 1;
}
/**
 * Show confirmation dialog to player and proceed the result in to the callback.
 * Allows to set a non-constant button text label.
 * 
 * @param int playerid:
 * @param string content: The text to show in the dialog window
 * @param string callback: Callback name where proceed result to
 * @param string oktext: Confirm button text label
 * @param string notext: Cancel button text label
 * @param int extraid: Extra variable
 */
stock ReturnUser(playerid, string:content[], string:callback[]) {
    SetPVarString(playerid, "DialogCallback", callback);
    Dialog_ShowCallback(playerid, using callback ReturnUserCallback, DIALOG_STYLE_INPUT, "Enter a player", content, "OK", "Cancel");
}
forward ReturnUserCallback(playerid, dialogid, response, listitem, string:inputtext[]);
public ReturnUserCallback(playerid, dialogid, response, listitem, string:inputtext[]) {
    new ply = INVALID_PLAYER_ID, callback[32];
    sscanf(inputtext, "u", ply);
    GetPVarString(playerid, "DialogCallback", callback, sizeof(callback));
    CallLocalFunction(callback, "iii", playerid, ply, response);
    return 1;
}
/**
 * New offer dialog
 * 
 * @param int playerid:
 * @param int targetid:
 * @param string playertext:
 * @param string targettext:
 * @param string callback: Callback name where proceed result to
 * 
 */
stock CreateOffer(playerid, targetid, string:playertext[], string:targettext[], string:callback[]) {
    // playerid
    SetPVarInt(playerid, "_offerTo", targetid);
    Dialog_ShowCallback(playerid, using callback OfferCalcelDialog, DIALOG_STYLE_MSGBOX, "Offer", playertext, "Cancel", "");
    // targetid
    SetPVarInt(targetid, "_offerFrom", playerid);
    SetPVarString(targetid, "DialogCallback", callback);
    Dialog_ShowCallback(targetid, using callback OfferResponseDialog, DIALOG_STYLE_MSGBOX, "Offer", targettext, "Accept", "Decline");
    return;
}
forward OfferResponseDialog(playerid, dialogid, response, listitem, string:inputtext[]);
public OfferResponseDialog(playerid, dialogid, response, listitem, string:inputtext[]) {
    new offerFrom = GetPVarInt(playerid, "_offerFrom");
    if(IsPlayerConnected(offerFrom) 
    && GetPVarInt(offerFrom, "_offerTo") == playerid) {
        new callback[50], string[OUTPUT];
        GetPVarString(playerid, "DialogCallback", callback, sizeof(callback));
        new success = CallLocalFunction(callback, "iii", offerFrom, playerid, response);
        if(success) { 
            format(string, sizeof(string), "{FFFFFF}%s (%d) has {00FF00}accepted{FFFFFF} your offer!", GetPlayerNameEx(playerid), playerid);
            ShowPlayerDialog(offerFrom, 0, DIALOG_STYLE_MSGBOX, "Offer accepted", string, "Close", "");
        }
        else {
            format(string, sizeof(string), "{FFFFFF}%s (%d) has {FF0000}declined{FFFFFF} your offer!", GetPlayerNameEx(playerid), playerid);
            ShowPlayerDialog(offerFrom, 0, DIALOG_STYLE_MSGBOX, "Offer canceled", string, "Close", "");
        }
    }
    DeletePVar(playerid, "DialogCallback");
    DeletePVar(playerid, "_offerFrom");
    DeletePVar(offerFrom, "_offerTo");
}
forward OfferCalcelDialog(playerid, dialogid, response, listitem, string:inputtext[]);
public OfferCalcelDialog(playerid, dialogid, response, listitem, string:inputtext[]) {
    new offerTo = GetPVarInt(playerid, "_offerTo");
    ShowPlayerDialog(offerTo, 0, DIALOG_STYLE_MSGBOX, "Offer canceled", "����������� ��������!", "Close", "");
    DeletePVar(offerTo, "DialogCallback");
    DeletePVar(offerTo, "_offerFrom");
    DeletePVar(playerid, "_offerTo");
}
