/**
 * List include files of headers
 * @author Moroz <moroz@malefics.com>
 * @author Jonathan_Rosewood <jonathan-rosewood@yandex.ru>
 *
 * Example of major headers:
 * #include <gamemode/functions/filename.inc>
 *
 * Example of minor headers:
 * #tryinclude <gamemode/functions/filename.inc>
 *
 */

#tryinclude <NewDawn/headers/admin.inc>
#tryinclude <NewDawn/headers/anticheat.inc>
#tryinclude <NewDawn/headers/inventory.inc>
#tryinclude <NewDawn/headers/vehicle_inventory.inc>
#tryinclude <NewDawn/headers/vehicle_tuning.inc>
#tryinclude <NewDawn/headers/vehicles.inc>
#tryinclude <NewDawn/headers/factions.inc>
#tryinclude <NewDawn/headers/property.inc>
#tryinclude <NewDawn/headers/prison.inc>
#tryinclude <NewDawn/headers/radio.inc>  
//#tryinclude <NewDawn/headers/drugs.inc> 
