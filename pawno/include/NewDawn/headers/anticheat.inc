/** 
 * Header file for anti cheating system 
 *
 * @author Moroz <igorsmorozov@gmail.com>
 */

#if defined anticheat_included || !defined admin_included
	#endinput
#endif

#define anticheat_included

new const Float:MAX_SPEED_ON_FOOT = 30.0; // Units/sec

enum HACK_TYPE {
	HACK_TYPE_NONE,
	HACK_TYPE_JETPACK,
	HACK_TYPE_AIRBRAKE,
	HACK_TYPE_MONEY, 
	HACK_TYPE_WEAPON
}; 


forward OnHackReported(playerid, HACK_TYPE:type, timestamp);