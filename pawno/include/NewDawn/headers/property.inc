/**
 * property.inc
 *
 * ������������ ���� ��� ������� ������������.
 * @author gim <myvrenik@gmail.com>
 *
 * �������:
 * 
 * @see StartPropertySaveTimer(Property, Seconds, bool:FullSave = false)
 * @see LoadProperty()
 * @see AddProperty(Property)
 * @see UpdateProperty(Property, bool:TextOnly = false)
 * @see ClearProperty(Property)
 * @see GetPlayerNearProperty(playerid, Float:Distance, bool:Enter, bool:Exit)
 * @see ShowPropertyStatsToPlayer(Property, playerid, bool:debugInfo = false)
 * @see GetPlayerPropertyCount()
 * @see SaveProperty(Property)
 * @see SaveBusinessStats(Property)
 */
 
#if defined __property_inc_included
    #endinput
#endif
#define __property_inc_included


#define P_COUNT             (50)     // ������������ ���-�� ������������ �� �������
#define P_MIN_VWORLD        (100)    // ������� � ������ ������������ ���� ������������ ����� ������������� ��� ����
#define P_3D_DISTANCE       (20.0)   // ��������� ��������� 3D ������ ����� �� ������ (������ ��� ��������)
#define P_PICKUP_DISTANCE   (150.0)  // ��������� ��������� ������ ����� �� ������ (������ ��� ��������)
#define P_CP_DISTANCE       (1.0)    // ��������� ��������� ��������� ����� �� ������  (������ ��� �����)
#define P_DEFAULT_VWORLD    (0)      // ID ������������ ����, � ������� ��������� ����
#define P_DEFAULT_INTERIOR  (0)      // ID ���������, � ������� ��������� ����

#define P_LIMIT_PER_PLAYER  (3)      // ������������ ���-�� ������������ �� ������

#define P_HOUSE_3D_COLOR    0xFFFF00EE
#define P_BIZZ_3D_COLOR     0x00FFFFEE

#define P_STATUS_ONSALE  (0)
#define P_STATUS_SOLD    (1)
#define P_STATUS_FROZEN  (2)

#define P_TYPE_HOUSE    (0)
#define P_TYPE_SHOP     (1)
#define P_TYPE_FOOD     (2)
#define P_TYPE_BAR      (3)
#define P_TYPE_SPRAY    (4)
#define P_TYPE_CARDEAL  (5)
#define P_TYPE_FUEL     (6)
#define P_TYPE_RENTCAR  (7)
#define P_TYPE_PRISON   (8)


#define MAX_FURNITURE 20

new prTypeName[][16] = {
    "house",
    "shop",
    "food",
    "bar",
    "spray",
    "dealership",
    "fuel",
    "rentcar",
	"prison"
};

enum furnInfo {
	furnID,
	furnObject, // dynamic object ID
	furnItem, // inventory item
	Float:furnPosX,
	Float:furnPosY,
	Float:furnPosZ,
	Float:furnRotX,
	Float:furnRotY,
	Float:furnRotZ,
	furnInterior,
	furnVirtualWorld
};
new Furniture[P_COUNT][MAX_FURNITURE][furnInfo];
new Iterator:PropFurniture[P_COUNT]<MAX_FURNITURE>;

new Iterator:Propertys<P_COUNT>;
new Iterator:Houses<P_COUNT>;
new Iterator:Business<P_COUNT>;

enum PropertyIntsEnum {
    Float:piPos[3],
    Float:piAngle,
    piInt,
    piRooms,
    piName[50]
}
new const HouseInts[][PropertyIntsEnum] = {
    {{1.1853,-3.2387,999.4284}, 90.00, 2, 1, "Trailer"},                                       // 1: Trailer
    {{260.7713,1285.3975,1080.2578}, 0.000, 4, 1, "Small house"},                              // 2: Small house
    {{244.0335,304.9802,999.1484}, 270.0, 1, 1, "Denise's Bedroom"},                           // 3: Denise's Bedroom
    {{266.7519,304.6744,999.1484}, 270.0, 2, 1, "Katie's house"},                              // 4: Katie's house
    {{2233.5835,-1113.8524,1050.8828}, 0.000, 5, 1, "Hotel Room"},                             // 5: Hotel Room
    {{223.6001,1288.0743,1082.1328}, 0.000, 1, 1, "Usual house with a bedroom"},               // 6: Usual house with a bedroom 
    {{443.9521,509.5001,1001.4195}, 270.0, 12, 1, "Huge motel room"},                          // 7: Huge motel room, 12
    {{292.7601,310.1764,999.1484}, 90.00, 3, 1, "Barn"},                                       // 8: Barn
    {{2466.7422,-1698.3252,1013.5078}, 90.00, 2, 1, "Ryder's house"},                          // 9: Ryder's house
    {{2524.8845,-1679.4692,1015.4986}, 270.0, 1, 1, "Sweet's house"},                          // 10: Sweet's house
    {{2263.0562,-1133.9366,1050.6328}, 180.0, 10, 2, "Hotel room"},                            // 11: Hotel room
    {{295.1390,1474.4700,1080.2578}, 0.000, 15, 2, "Two-room apartment"},                      // 12: Two-room apartment. Not cheap
    {{227.3565,1114.0134,1080.9984}, 270.0, 5, 4, "Two floors, crack den"},                    // 13: Two floors, crack den
    {{446.7592,1398.6642,1084.3047}, 0.000, 2, 2, "Huge house, two rooms"},                    // 14: Huge house, two rooms
    {{235.1090,1189.7700,1080.2578}, 0.000, 3, 3, "Two floors, rich house"},                   // 15: Two floors, rich house
    {{225.7421,1239.8827,1082.1406}, 90.00, 2, 1, "Regular ghetto house"},                     // 16: Regular ghetto house
    {{2350.2214,-1181.0660,1027.9766}, 90.00, 5, 4, "Burning Desire building, two floors"},    // 17: Burning Desire building, two floors
    {{2318.0330,-1025.7512,1050.2109}, 0.000, 9, 1, "Rich house, two floors"},                 // 18: Rich house, two floors
    {{227.7230,1114.3900,1080.9922}, 270.0, 5, 5, "Very rich house, two floors"}               // 19: Very rich house, two floors
};
new const BusinessInts[][PropertyIntsEnum] = { // ��� ������ �������, ��� �����.
    {{286.148987,-40.644398,1001.569946}, 0.0, 1, 0, "Ammunation 1"},                          // 1: Ammunation 1
    {{286.800995,-82.547600,1001.539978}, 0.0, 4, 0, "Ammunation 2"},                          // 2: Ammunation 2
    {{296.919983,-108.071999,1001.569946}, 0.0, 6, 0, "Ammunation 3"},                         // 3: Ammunation 3
    {{314.820984,-141.431992,999.661987}, 0.0, 7, 0, "Ammunation 4"},                          // 4: Ammunation 4
    {{316.524994,-167.706985,999.661987}, 0.0, 6, 0, "Ammunation 5"},                          // 5: Ammunation 5
    {{365.7158,-9.8873,1001.8516}, 0.0, 9, 0, "Cluckin` bell"},                                // 6: Cluckin` bell
    {{-25.884499,-185.868988,1003.549988}, 0.0, 17, 0, "24/7"},                                // 24/7: ; �������, �����������
    {{6.091180,-29.271898,1003.549988}, 0.0, 10, 0, "24/7"},                                   // 8: 27/7; ������
    {{-30.946699,-89.609596,1003.549988}, 0.0, 18, 0, "24/7"},                                 // 9: 27/7; ������
    {{-25.132599,-139.066986,1003.549988}, 0.0, 16, 0, "24/7"},                                // 10: 27/7; ������
    {{-27.312300,-29.277599,1003.549988}, 0.0, 4, 0, "24/7"},                                  // 11: 27/7; ������
    {{-26.691599,-55.714897,1003.549988}, 0.0, 6, 0, "24/7"},                                  // 12: 27/7; ������
    {{372.3520,-131.6510,1001.4922}, 0.0, 5, 0, "Well Stacked Pizza"},                         // 13: Well Stacked Pizza
    {{363.4129,-74.5786,1001.5078}, 0.0, 10, 0, "Burger Shot"},                                // 14: Burger Shot
    {{378.026,-190.5155,1000.6328}, 0.0, 17, 0, "Rusty Brown's Donuts"},                       // 15: Rusty Brown's Donuts
    {{501.980987,-69.150199,998.757812}, 0.0, 11, 0, "Ten Green Bottles Bar"},                 // 16: Ten Green Bottles Bar
    {{-227.027999,1401.229980,27.765625}, 0.0, 18, 0, "Lil' probe inn Bar"},                   // 17: Lil' probe inn Bar
    {{493.390991,-22.722799,1000.679687}, 0.0, 17, 0, "Club"},                                 // 18: Club
	{{1450.8230,-1579.3287,73.1900}, 0.0, 23, 0, "Bank"},                                      // 19: Bank
	{{1173.3900,-1323.2091,15.3924}, 270.0, 24, 0, "ASGH"},                                    // 20: ASGH 
	{{1173.3900,-1323.2091,15.3924}, 0.0, 25, 0, "LSPD"},                                      // 21: LSPD TODO
	{{1173.3900,-1323.2091,15.3924}, 0.0, 26, 0, "LSP"}                                        // 22: LSP TODO
};

enum prInfo {
    prID,
    prOwnerID,
    prOwner[MAX_PLAYER_NAME],
    Float:prEnter[4],
    prInt,
    prWorld,
    prDescript[100],
    prLock,
    prLocked,
    prAlarm,
    prAlarmed,
    prCost,
    prStatus,
    prType,
    // Business only:
    prProds,
    prMaxProds,
    prPriceLevel,
    prMoney,
    // --------------
    Text3D:pr3DText[2],
    prMarker,
    bool:prUpdated
};
new PropertyInfo[P_COUNT][prInfo];

enum prEdit {
    eOwner,
    eDescription,
    eInterior,
    ePosition
}

new bool:PlayerPrIntChange[MAX_PLAYERS] = false;
new Text:PropertyIntChangeInfo[MAX_PLAYERS];
