/**
 * vehicles.inc
 *
 * ������������ ���� ��� ������� �����.
 * @author Moroz <moroz@malefics.com>
 * @author Impereal 
 * @version 1.0
 *
 * �������:
 * @see ToggleVehicleMenuForPlayer(playerid, bool:toggle)
 * @see SaveVehicle(veh)
 * @see GetSpawnedVehicleSlot(playerid)
 * @see SortPlayerVehicles(playerid)
 * @see GetPlayerFreeVehicleSlot(playerid)
 * @see ShowVehiclePurchaseDialog(playerid, page)
 */

#if defined vehicles_included
	#endinput
#endif
#define vehicles_included



#define MAX_PLAYER_VEHICLES 5
#define VEHICLES_PER_PAGE 15
new SALE_VEHICLES = 0;


forward BreakInVehicle(playerid, bool:cancelled);

forward PurchaseConfirmation(playerid, dialogid, response, listitem, string:inputtext[]);
forward PageDialog(playerid, dialogid, response, listitem, string:inputtext[]);

forward LoadVehicles(query[], playerid, carslot, connectionHandle);
forward LoadPlayerVehiclesInfo(query[], playerid, extraid, connectionHandle);
forward LoadData_Vehicles(query[], playerid, extraid, connectionHandle);
forward CreateNewVehicle(query[], playerid, carslot, connectionHandle);

enum vInfo {
	vID,
	vOwner,
	vOwnerName[24],
	vPlate[12], 
	vModel,
	vColor1,
	vColor2,
	vPaintJob,
	vComponents[14],
	Float:vHealth,
	vDamageStatus[4],
	Float:vPosX,
	Float:vPosY,
	Float:vPosZ,
	Float:vRotZ,
	vInt,
	vVW,
	vCreatedTime,
	vFlags[6],
	vInvItem[VEHICLE_TRUNK_SIZE],
	vInvQuantity[VEHICLE_TRUNK_SIZE],
	vInvSerial[VEHICLE_TRUNK_SIZE],
	vLock,
	vAlarm,
	vImmob,
	vRadio,
	vTunedRadio, // current radio station
	vFaction,
	vInsurance,
	vDeaths, 
	vDisabled,
	vOwnerID,
	Float:vFuel, // in Liters
	Float:vMileage // in KMs
};
new Vehicles[MAX_VEHICLES+1][vInfo];

enum PVehInfo {
	pvID,
	pvCar,
	pvModel,
	pvLock,
	pvImmob,
	pvAlarm,
	pvInsurance,
	pvDeaths
};

new PVeh[MAX_PLAYERS][MAX_PLAYER_VEHICLES][PVehInfo];

enum	// Vehicle types
{
		VEH_TYPE_PASSENGER_CAR,
		VEH_TYPE_OFF_ROAD,
		VEH_TYPE_TRUCK, 
		VEH_TYPE_BIKE, 
		VEH_TYPE_MOTORBIKE,
		VEH_TYPE_BOATS, 
		VEH_TYPE_AIRPLANE,
		VEH_TYPE_TRAIN
};

enum VEH_CHARACTERISTICS
{	
		V_C_NAME[32],			// Vehicle name
		V_C_TYPE,				// Vehicle type
		V_C_TANK, 				// Gas tank capacity
		Float:V_C_CONSUMPTION,	// Fuel consumption (per km)
		V_C_WHEELS_AMOUNT,		// Number of wheels
		V_C_MASS,				// Vehicle weight
		V_C_TRUNK_SIZE,			// Trunk size
		V_C_PRICE               // Price (if for sale). 0 - not for sale. At all.
}; 
new VehInfo[212][VEH_CHARACTERISTICS];


new const VehicleBreakoutTime[5] = {
	120, 240, 480, 600, 720
};
