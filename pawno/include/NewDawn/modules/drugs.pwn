/**
 * drugs.pwn
 *
 * �������� ���� ������� ����������
 * @author gim <myvrenik@gmail.com>
 * @version 0.2
 *
 * Player functions:
 * @see SetPlayerDrugEffects(playerid, effects[], eCount)
 * @see ClearPlayerDrugEffects(playerid)
 *
 * Callbacks:
 * @see OnPlayerUseDrug(playerid, drTypes:type)
 *
 */
 
#if !defined __drugs_inc_included 
    #endinput
#endif

#include <YSI\y_text>
#include <YSI\y_iterate>
#include <YSI\y_hooks>
loadtext Core[Commands], Drugs[Messages];

hook OnPlayerDeath(playerid, killerid, reason) {
    ClearPlayerDrugEffects(playerid);
}

//----------------------------------------------------------------------------------------------------------
/**
 * Starts visual drug effects for player
 *
 * @param playerid int:
 * @param effects[] int: The list array of effects to apply
 * @param eCount int: The number of effects in the list (effects[])
 */
stock SetPlayerDrugEffects(playerid, effects[], eCount) {
    for(new i = 0; i < eCount; i++) {
        switch(effects[i]) {
            case eDrunk: SetPlayerDrunkLevel(playerid, 4000);
            case eGreenWeather: {
                if(WeatherEffectsAllowed) {
                    SetPlayerWeather(playerid, -1);
                    PlayerWeatherLock(playerid);
                }
                defer ClearPlayerDrugEffects(playerid);
            }
            case eRedWeather: {
                if(WeatherEffectsAllowed) {
                    SetPlayerWeather(playerid, -14);
                    PlayerWeatherLock(playerid);
                }
                defer ClearPlayerDrugEffects(playerid);
            }
            case eCrazyWeather: {
                if(WeatherEffectsAllowed) {
                    SetPlayerWeather(playerid, -30);
                    PlayerWeatherLock(playerid);
                }
                defer ClearPlayerDrugEffects(playerid);
            }
            case eRedWindWeather: {
                if(WeatherEffectsAllowed) {
                    SetPlayerWeather(playerid, -66);
                    PlayerWeatherLock(playerid);
                }
                defer ClearPlayerDrugEffects(playerid);
            }
            case eDynamicWeather: {
                if(WeatherEffectsAllowed) {
                    SetPlayerTime(playerid, 23,0);
                    SetPlayerWeather(playerid, 1337);
                    PlayerWeatherLock(playerid);
                }
                defer ClearPlayerDrugEffects(playerid);
            }
            case eGlares: SendClientMessage(playerid, 0x00FF00FF, "TODO: eGlares");
            case eRandomObjects: SendClientMessage(playerid, 0x00FF00FF, "TODO: eRandomObjects");
            case eLights: SendClientMessage(playerid, 0x00FF00FF, "TODO: eLights");
            case eSounds: defer PlayPlayerRandomDrugSound[(5+random(25))*1000](playerid);
        }
    }
    return;
}
/**
 * Should be called when player uses drugs to apply all effects
 *
 * @param playerid int:
 * @param type int: The drug type
 */
stock OnPlayerUseDrug(playerid, drTypes:type) {
    switch(type){
        case tCannabis: {
            SetPlayerDrugEffects(playerid, {eDrunk}, 1);
            SetPlayerSpecialActionEx(playerid, SPECIAL_ACTION_SMOKE_CIGGY);
        }
        case tCocaine: {
            SetPlayerDrugEffects(playerid, {eGreenWeather}, 1);
        }
        case tCrack: {
            SetPlayerDrugEffects(playerid, {eGreenWeather}, 1);
        }
        case tHeroine: {
            SetPlayerDrugEffects(playerid, {eRedWindWeather}, 1);
        }
        case tLSD: {
            SetPlayerDrugEffects(playerid, {eRedWeather, eGlares, eRandomObjects, eLights, eSounds}, 5);
        }
        case tMeth: {
            SetPlayerDrugEffects(playerid, {eGreenWeather}, 1);
        }
        default: return;
    }
    SetPVarInt(playerid, "PlayerOnDrugs", 1);
    SetPVarInt(playerid, "DrugAddictLevel", GetPVarInt(playerid, "DrugAddictLevel")+DrugInfo[type][drAddictive]);
    KillTimer(CurrentPlayerDrugTimer[playerid]);
    CurrentPlayerDrugTimer[playerid] = SetTimerEx_("DrugChangePlayerHealthTimer", 
                                                   DrugInfo[type][drHealthIncTime]*1000, DrugInfo[type][drHealthIncTime]*1000, 
                                                   DrugInfo[type][drHealthIncFullTime]/DrugInfo[type][drHealthIncTime], 
                                                   "ii", playerid, DrugInfo[type][drHealthInc]);
    return;
}
/**
 * Makes player health change textdraw with fade effect
 *
 * @param playerid int:
 * @param amount int: The amount of health
 */
forward DrugChangePlayerHealthTimer(playerid, amount);
public DrugChangePlayerHealthTimer(playerid, amount) {
    new Float:Health;
    GetPlayerHealth(playerid, Health);
    if(Health+amount > 100.0 && amount > 0
    || Health+amount < 20 && amount < 0) {
        KillTimer(CurrentPlayerDrugTimer[playerid]);
        return 0;
    }
    SetPlayerHealth(playerid, Health+amount);
    defer DrugChangePlayerHealthTD(playerid, amount);
    return 1;
}
timer DrugChangePlayerHealthTD[50](playerid, amount) { 
    static PlayerText:DRPHTD_TD[MAX_PLAYERS], DRPHTD_COLOR[MAX_PLAYERS], DRPHTD_CC[MAX_PLAYERS];
    if(DRPHTD_CC[playerid] == 0) {
        new string[5];
        if(amount < 0) {
            format(string, sizeof(string), "%d", amount);
            DRPHTD_COLOR[playerid] = 0xFF0000FF;
        }
        else {
            format(string, sizeof(string), "+%d", amount);
            DRPHTD_COLOR[playerid] = 0x00FF00FF;
        }
        DRPHTD_TD[playerid] = CreatePlayerTextDraw(playerid, 300.0, 150.0, string);
        PlayerTextDrawFont(playerid, DRPHTD_TD[playerid], 1);
        PlayerTextDrawLetterSize(playerid, DRPHTD_TD[playerid], 1, 4);
        PlayerTextDrawColor(playerid, DRPHTD_TD[playerid], DRPHTD_COLOR[playerid]);
        PlayerTextDrawSetOutline(playerid, DRPHTD_TD[playerid], false);
        PlayerTextDrawSetProportional(playerid, DRPHTD_TD[playerid], true);
        PlayerTextDrawSetShadow(playerid, DRPHTD_TD[playerid], 0);
        PlayerTextDrawShow(playerid, DRPHTD_TD[playerid]);
    }
    DRPHTD_CC[playerid]++;
    if(DRPHTD_CC[playerid] < 16) {
        DRPHTD_COLOR[playerid] -= 0x00000011;
        PlayerTextDrawHide(playerid, DRPHTD_TD[playerid]);
        PlayerTextDrawColor(playerid, DRPHTD_TD[playerid], DRPHTD_COLOR[playerid]);
        PlayerTextDrawShow(playerid, DRPHTD_TD[playerid]);
        defer DrugChangePlayerHealthTD(playerid, amount);
    }
    else {
        PlayerTextDrawHide(playerid, DRPHTD_TD[playerid]);
        PlayerTextDrawDestroy(playerid, DRPHTD_TD[playerid]);
        //DRPHTD_TD[playerid] = INVALID_TEXT_DRAW; // warning 213: tag mismatch ?
        DRPHTD_CC[playerid] = 0;
    }
}
/**
 * Random sound effects
 *
 * @param playerid int:
 */
timer PlayPlayerRandomDrugSound[10000](playerid) {
    SendClientMessage(playerid, 0x00FF00FF, "Note that these random sounds was chosen for testing purposes and will be replaced by more appropriate sounds soon");
    PlayAudioStreamForPlayer(playerid, DrugSoundsURL[random(sizeof(DrugSoundsURL))]);
}
/**
 * Clears all player drug effects
 *
 * @param playerid int:
 */
timer ClearPlayerDrugEffects[600000](playerid) { // 600000 ms = 10 min
    if(WeatherEffectsAllowed) {
        PlayerWeatherUnLock(playerid);
        SetPlayerTime(playerid, 12, 0);
    }
    DeletePVar(playerid, "PlayerOnDrugs");
    KillTimer(CurrentPlayerDrugTimer[playerid]);
    return;
}
/**
 * This timer checks drug addict level and removes health every time if it's highter than 10
 */
task SideEffects[600000]()  { // 600000 ms = 10 min
    static tmp[MAX_PLAYERS] = 0;
    new plDrugAddictLevel;
    foreach(new i : Player) {
        if(GetPVarInt(i, "PlayerOnDrugs")) continue;
        plDrugAddictLevel = GetPVarInt(i, "DrugAddictLevel");
        if(plDrugAddictLevel >= 10 && plDrugAddictLevel < 20) {
            if(tmp[i] >= 2) {
                CurrentPlayerDrugTimer[i] = SetTimerEx_("DrugChangePlayerHealthTimer", 
                                                   (10+random(11))*1000, (10+random(11))*1000, 20, 
                                                   "ii", i, -3);
                tmp[i] = 0;
                Text_Send(i, $SIDE_EFFECT_MSG);
            }
            else tmp[i]++;
        }
        else if(plDrugAddictLevel >= 20) {
            CurrentPlayerDrugTimer[i] = SetTimerEx_("DrugChangePlayerHealthTimer", 
                                                   (10+random(11))*1000, (10+random(11))*1000, 20, 
                                                   "ii", i, -3);
            Text_Send(i, $SIDE_EFFECT_MSG);
        }
    }
}
//----------------------------------------------------------------------------------------------------------

// Temporary commands for testing purposes:
CMD:drugme(playerid, params[]) {
    if(GetPVarInt(playerid, "AdminLevel") < ADMIN_LEVEL_LEAD) return true;
    if(isnull(params)) {
        Text_Send(playerid, $COMMAND_SYNTAX, YCMD:drugme, "<id>");
        return true;
    }
    new drugID;
    if(!sscanf(params, "i", drugID)) {
        OnPlayerUseDrug(playerid, drTypes:drugID);
        new string[OUTPUT];
        format(string, sizeof(string), "You used %s", DrugInfo[drTypes:drugID][drName]);
        SendClientMessage(playerid, 0x00FF00FF, string);
    }
    return true;
}
