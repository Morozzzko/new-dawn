#include <NewDawn/functions/special_actions.inc>

enum {
    // 24/7:
    B_ITEM_TYPE_SPRAY,
    B_ITEM_TYPE_GOLF,
    B_ITEM_TYPE_FLOWERS,
    B_ITEM_TYPE_BAT,
    B_ITEM_TYPE_SHOVEL,
    B_ITEM_TYPE_CUE,
    // Bar:
    B_ITEM_TYPE_BEER,
    B_ITEM_TYPE_VODKA,
    B_ITEM_TYPE_WINE,
    B_ITEM_TYPE_COCTAIL,
    B_ITEM_TYPE_ABSENT,
    B_ITEM_TYPE_VHISKEY
}

enum BuyEnum {
    bName[20],
    bCost,
    bProd,
    bItemType
}
new const BuyListShop[][BuyEnum] = {
    // 24/7:
    {"�������� � �������", 9, 7, B_ITEM_TYPE_SPRAY}, {"������", 8, 8, B_ITEM_TYPE_GOLF}, 
    {"�����", 7, 9, B_ITEM_TYPE_FLOWERS}, {"�. ����", 6, 10, B_ITEM_TYPE_BAT}, 
    {"������", 5, 11, B_ITEM_TYPE_SHOVEL}, {"���", 4, 12, B_ITEM_TYPE_CUE}
};
new const BuyListBar[][BuyEnum] = {
    // ���:
    {"����", 4, 1, B_ITEM_TYPE_BEER}, {"�����", 5, 2, B_ITEM_TYPE_VODKA}, 
    {"����", 6, 3, B_ITEM_TYPE_WINE}, {"��������", 7, 4, B_ITEM_TYPE_COCTAIL}, 
    {"������", 8, 5, B_ITEM_TYPE_ABSENT}, {"�����", 9, 6, B_ITEM_TYPE_VHISKEY}
};

stock OnPlayerBuyingSomething(playerid, Property, Cost, Products, BuyingItemType) {
    if(GetPlayerMoney(playerid) < Cost) {
        SendClientMessage(playerid, COLOR_NOTIFICATION, "������������ �����!");
        return 0;
    }
    if(PropertyInfo[Property][prProds] < Products) {
        SendClientMessage(playerid, COLOR_NOTIFICATION, "���������� ������ ��� � �������!");
        return 0;
    }
    switch(BuyingItemType) {
        // 24/7:
        case B_ITEM_TYPE_SPRAY: GivePlayerWeapon(playerid, 41, 200);
        case B_ITEM_TYPE_GOLF: GivePlayerWeapon(playerid, 2, 1);
        case B_ITEM_TYPE_FLOWERS: GivePlayerWeapon(playerid, 14, 1);
        case B_ITEM_TYPE_BAT: GivePlayerWeapon(playerid, 5, 1);
        case B_ITEM_TYPE_SHOVEL: GivePlayerWeapon(playerid, 6, 1);
        case B_ITEM_TYPE_CUE: GivePlayerWeapon(playerid, 7, 1);
        // Bar:
        case B_ITEM_TYPE_BEER: SetPlayerSpecialActionEx(playerid, SPECIAL_ACTION_DRINK_BEER);
        case B_ITEM_TYPE_VODKA: SetPlayerSpecialActionEx(playerid, SPECIAL_ACTION_DRINK_VODKA);
        case B_ITEM_TYPE_WINE: SetPlayerSpecialActionEx(playerid, SPECIAL_ACTION_DRINK_WINE);
        case B_ITEM_TYPE_COCTAIL: SetPlayerSpecialActionEx(playerid, SPECIAL_ACTION_DRINK_SPRUNK);
        case B_ITEM_TYPE_ABSENT: SetPlayerSpecialActionEx(playerid, SPECIAL_ACTION_DRINK_WINE2);
        case B_ITEM_TYPE_VHISKEY: SetPlayerSpecialActionEx(playerid, SPECIAL_ACTION_DRINK_WINE3);
        
        default: return SendClientMessage(playerid, COLOR_ERROR, "��������� ������: ������������� ������� �� ������. ����������, �������� �� ���� �� ����� ������!");
    }
    GivePlayerMoney(playerid, -Cost);
    PropertyInfo[Property][prProds] -= Products;
    SendClientMessage(playerid, COLOR_NOTIFICATION, "����������� � ��������!");
    return 1;
}

stock OnPlayerSellingProds(playerid, Property) {
    return 1;
}

YCMD:buy(playerid, params[], help) {
    new Property = GetPlayerNearProperty(playerid, 40.0, false, true);
    if(Property == -1) return SendClientMessage(playerid, COLOR_NOTIFICATION, "��� ������ ��������!");
    
    new string[OUTPUT];
    switch(PropertyInfo[Property][prType]) {
        case P_TYPE_BAR: for(new i = 0; i < sizeof(BuyListBar); i++) format(string, sizeof(string), "%s%s\n", string, BuyListBar[i][bName]);
        case P_TYPE_SHOP: for(new i = 0; i < sizeof(BuyListShop); i++) format(string, sizeof(string), "%s%s\n", string, BuyListShop[i][bName]);
        default: return SendClientMessage(playerid, COLOR_NOTIFICATION, "��� ������ ��������!");
    }
    if(isnull(string)) return SendClientMessage(playerid, COLOR_NOTIFICATION, "��� ������ ��������!");
    
    SetPVarInt(playerid, "_PropertyID", Property);
    Dialog_ShowCallback(playerid, using callback BuyDialogResponse, DIALOG_STYLE_LIST, "{FF0000}���� �������", string, "������", "�����");
    return true;
}

forward BuyDialogResponse(playerid, dialogid, response, listitem, string:inputtext[]);
public BuyDialogResponse(playerid, dialogid, response, listitem, string:inputtext[]) {
    //#pragma unused dialogid
    new Property = GetPVarInt(playerid, "_PropertyID");
    if(response) {
        switch(PropertyInfo[Property][prType]) {
            case P_TYPE_BAR: OnPlayerBuyingSomething(playerid, Property, BuyListBar[listitem][bCost]*PropertyInfo[Property][prPriceLevel], BuyListBar[listitem][bProd], BuyListBar[listitem][bItemType]);
            case P_TYPE_SHOP: OnPlayerBuyingSomething(playerid, Property, BuyListShop[listitem][bCost]*PropertyInfo[Property][prPriceLevel], BuyListShop[listitem][bProd], BuyListShop[listitem][bItemType]);
            default: return SendClientMessage(playerid, COLOR_ERROR, "��������� ������!");
        }
    }
    return 1;
}
