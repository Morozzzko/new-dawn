/**
 * vehicles.pwn
 *
 * �������� ���� ������� ����������
 * @author Moroz <moroz@malefics.com>
 * @version 1.0
 *
 * �������:
 * @see YCMD:vehicle
 */
 
 #if !defined vehicles_included
	#endinput
#endif

#include <YSI\y_text>

loadtext Vehicles[Errors], Vehicles[Messages], Core[Commands], Core[Errors], Factions[Errors];




/**
 * Reloads player vehicle list
 */
stock ReloadPlayerVehicleList(playerid) {
	new string[128];
    mysql_format(MySQL, string, "SELECT `id`, `Model`, `Lock`, `Alarm`, `Immobilizer`, `Insurance`, `Deaths` FROM \
        `vehicles` WHERE `owner`='%d' LIMIT %d", GetPVarInt(playerid, "ID"), MAX_PLAYER_VEHICLES);
    mysql_query_callback(playerid, string, "LoadPlayerVehiclesInfo", 0, MySQL, true);
	return 1;
}

/**
 * ������� ������ ������� ���� ������� � N ��������
 *
 * @param int ID ������
 * @param int ����� �������� (1 � �����)
 */
 
stock ShowVehiclePurchaseDialog(playerid, page) {
	new string[512], count = 0;
	for(new i = VEHICLES_PER_PAGE*(page-1);i<sizeof(VehInfo) && count < VEHICLES_PER_PAGE*page && count < SALE_VEHICLES; i++) {
		if(VehInfo[i][V_C_PRICE] == 0) continue;
		if(++count >= VEHICLES_PER_PAGE*(page-1))
			format(string, sizeof(string), "%s%d \"%s\"\t $%d\n", string, i+400, VehInfo[i][V_C_NAME], VehInfo[i][V_C_PRICE]);
	}
	if(count%VEHICLES_PER_PAGE == 0 && count != SALE_VEHICLES)
		strcat(string, "Next\n");
	if(page != 1)
		strcat(string, "Previous\n");
	strmid(string, string, 0, strlen(string)-1);
	SetPVarInt(playerid, "DialogPage", page);
	Dialog_ShowCallback(playerid, using callback PageDialog, DIALOG_STYLE_LIST, "Vehicle dealership", string, "Purchase", "Cancel");
	return 1;
}

/**
 * ���������� ��������� ���� ��� ������.
 *
 * @param int ID ������
 * @return int ���� ������ ��� -1 ���� �� ���
 */
 
stock GetPlayerFreeVehicleSlot(playerid) {
	for(new i=0;i<MAX_PLAYER_VEHICLES;i++) {
		if(PVeh[playerid][i][pvID] == 0) return i;
	}
	return -1;
}

/**
 * ��������� ������ �����
 *
 * @param int ID ������
 */

stock SortPlayerVehicles(playerid) {
	for(new i=0;i<MAX_PLAYER_VEHICLES-1;i++) {
		if(PVeh[playerid][i][pvID] == 0) {
			PVeh[playerid][i] = PVeh[playerid][i+1];
			PVeh[playerid][i+1][pvID] = 0;
		}
	}
}

/**
 * ���������� ���� ������� ����������� ������ � ������
 *
 * @param int ID ������
 * @return int ���� ������ (-1 ���� ��������)
 */

stock GetSpawnedVehicleSlot(playerid) {
	return GetPVarInt(playerid, "SpawnedVehicleSlot")-1;
}

/**
 * ��������� ������ � ���� ������
 * @since 1.0
 * @param veh ID ������ �� ������� (������ ���� ���������)
 */

stock SaveVehicle(veh) {
	new string[1500], items[170], quantity[250], serial[370];
	new engine, lights[2], alarm, doors[2], bonnet, boot, objective, panels, tires;
	if(veh < 0 || veh > MAX_VEHICLES || !Vehicles[veh][vID]) 
		return;
	GetVehicleHealth(veh, Vehicles[veh][vHealth]);
	GetVehicleParamsEx(veh,  engine, lights[0], alarm, doors[0], bonnet, boot, objective);
	GetVehicleDamageStatus(veh,panels,doors[1],lights[1],tires);
	GetVehiclePos(veh, Vehicles[veh][vPosX], Vehicles[veh][vPosY],Vehicles[veh][vPosZ]);  
	GetVehicleZAngle(veh, Vehicles[veh][vRotZ]);
	for(new i=0;i<VehInfo[Vehicles[veh][vModel]-400][V_C_TRUNK_SIZE] && i < VEHICLE_TRUNK_SIZE; i++) {
		if(Vehicles[veh][vInvItem][i] == 0) break;
		format(items, sizeof(items), "%s%d ", items, Vehicles[veh][vInvItem][i]);
		format(quantity, sizeof(quantity), "%s%d ", quantity, Vehicles[veh][vInvQuantity][i]);
		format(serial, sizeof(serial), "%s%d ", serial, Vehicles[veh][vInvSerial][i]);
	}
	format(string, sizeof(string), 
		"UPDATE `vehicles` SET `Health`='%f', `Flags`='%d %d %d %d %d %d', `DamageStatus`='%d %d %d %d',\
		`InvItems`='%s', `InvQuantity`='%s', `InvSerial`='%s',\
		`Fuel` = '%f', `Mileage`='%f', `PosX`='%f', `PosY`='%f', `PosZ`='%f', `RotZ`='%f', `Interior`='%d', `VirtualWorld`='%d'\
		WHERE `id`='%d'",
		Vehicles[veh][vHealth], 
		engine, lights[0], alarm, doors[0], bonnet, boot, 
		panels,doors[1],lights[1],tires, 
		items, quantity, serial, 
		Vehicles[veh][vFuel], Vehicles[veh][vMileage],
		Vehicles[veh][vPosX], Vehicles[veh][vPosY],Vehicles[veh][vPosZ],Vehicles[veh][vRotZ],
		Vehicles[veh][vInt], Vehicles[veh][vVW],	
		Vehicles[veh][vID]);
	mysql_query(string, MySQL);
}

public LoadVehicles(query[], playerid, carslot, connectionHandle) {

	new rows=0, fields=0;
	cache_get_data(rows, fields, connectionHandle);
	if(!rows) {
	    return 1;
	}
    new string[370], tmp[vInfo], veh = 0;
	for(new i=0;i < rows;i++) {
	
		cache_get_field_content(i,  "id", string, connectionHandle);
		tmp[vID] = strval(string);
		cache_get_field_content(i,  "Owner", string, connectionHandle);
		tmp[vOwner] = strval(string);
		cache_get_field_content(i,  "OwnerName", tmp[vOwnerName], connectionHandle);
		cache_get_field_content(i,  "Plate", tmp[vPlate], connectionHandle);
		cache_get_field_content(i,  "Model", string, connectionHandle);
		tmp[vModel] = strval(string);
		cache_get_field_content(i,  "Color1", string, connectionHandle);
		tmp[vColor1] = strval(string);
		cache_get_field_content(i,  "Color2", string, connectionHandle);
		tmp[vColor2] = strval(string);
		cache_get_field_content(i,  "PaintJob", string, connectionHandle);
		tmp[vPaintJob] = strval(string);
		cache_get_field_content(i,  "Components", string, connectionHandle);
		sscanf(string, "a<i>[14]", tmp[vComponents]);
		cache_get_field_content(i,  "Health", string, connectionHandle);
		tmp[vHealth] = floatstr(string);
		cache_get_field_content(i,  "DamageStatus", string, connectionHandle);
		sscanf(string, "a<i>[4]", tmp[vDamageStatus]);
		
		cache_get_field_content(i,  "PosX", string, connectionHandle);
		tmp[vPosX] = floatstr(string);
		cache_get_field_content(i,  "PosY", string, connectionHandle);
		tmp[vPosY] = floatstr(string);
		cache_get_field_content(i,  "PosZ", string, connectionHandle);
		tmp[vPosZ] = floatstr(string);
		cache_get_field_content(i,  "RotZ", string, connectionHandle);
		tmp[vRotZ] = floatstr(string);
		cache_get_field_content(i,  "Interior", string, connectionHandle);
		tmp[vInt] = strval(string);
		cache_get_field_content(i,  "VirtualWorld", string, connectionHandle);
		tmp[vVW] = strval(string);
		cache_get_field_content(i,  "CreatedTime", string, connectionHandle);
		tmp[vCreatedTime] = strval(string);
		cache_get_field_content(i,  "Flags", string, connectionHandle);
		sscanf(string, "a<i>[6]", tmp[vFlags]);
		
		cache_get_field_content(i,  "Lock", string, connectionHandle);
		tmp[vLock] = strval(string);
		cache_get_field_content(i,  "Alarm", string, connectionHandle);
		tmp[vAlarm] = strval(string);
		cache_get_field_content(i,  "Immobilizer", string, connectionHandle);
		tmp[vImmob] = strval(string);
		cache_get_field_content(i,  "Radio", string, connectionHandle);
		tmp[vRadio] = strval(string);
		tmp[vTunedRadio] = 0;
		
		cache_get_field_content(i,  "Faction", string, connectionHandle);
		tmp[vFaction] = strval(string);
		cache_get_field_content(i,  "Insurance", string, connectionHandle);
		tmp[vInsurance] = strval(string);
		cache_get_field_content(i,  "Deaths", string, connectionHandle);
		tmp[vDeaths] = strval(string);
		
		cache_get_field_content(i,  "Disabled", string, connectionHandle);
		tmp[vDisabled] = strval(string);
		
		
		cache_get_field_content(i,  "Fuel", string, connectionHandle);
		tmp[vFuel] = strval(string);
		
		cache_get_field_content(i,  "Mileage", string, connectionHandle);
		tmp[vMileage] = strval(string);
		
		
		
		tmp[vOwnerID] = INVALID_PLAYER_ID;
		
		veh = CreateVehicle(tmp[vModel], tmp[vPosX], tmp[vPosY], tmp[vPosZ], tmp[vRotZ], tmp[vColor1], tmp[vColor2], 60000);
		SetVehicleNumberPlate(veh, tmp[vPlate]);
		ChangeVehiclePaintjob(veh, tmp[vPaintJob]);
		for(new k=0;k<13;k++)
			if(tmp[vComponents][k] != 0) 
				AddVehicleComponent(veh, tmp[vComponents][k]);
		SetVehicleVirtualWorld(veh, tmp[vVW]);
		LinkVehicleToInterior(veh, tmp[vInt]);
		SetVehicleHealth(veh, tmp[vHealth]);
		UpdateVehicleDamageStatus(veh, tmp[vDamageStatus][0], tmp[vDamageStatus][1], tmp[vDamageStatus][2], tmp[vDamageStatus][3]);
		SetVehicleParamsEx(veh, tmp[vFlags][0], tmp[vFlags][1], tmp[vFlags][2], tmp[vFlags][3], tmp[vFlags][4], tmp[vFlags][5], 0);   
		
		cache_get_field_content(i,  "InvItems", string, connectionHandle);
		sscanf(string, "p<;>A<i>(0)[40]", tmp[vInvItem]);
		cache_get_field_content(i,  "InvQuantity", string, connectionHandle);
		sscanf(string, "p<;>A<i>(0)[40]", tmp[vInvQuantity]);
		cache_get_field_content(i,  "InvSerial", string, connectionHandle);
		sscanf(string, "p<;>A<i>(0)[40]", tmp[vInvSerial]);
		
		Vehicles[veh] = tmp;
		Vehicle_SortInventory(veh); 
		playerid--;
		if(playerid > -1) {
			PVeh[playerid][carslot][pvCar] = veh;
			Vehicles[veh][vOwnerID] = playerid;
			Text_Send(playerid, $VEHICLE_SPAWNED);
		}
	}
	return 1;
}

public CreateNewVehicle(query[], playerid, carslot, connectionHandle) {
	if(!LoggedIn(playerid)) 
		return;
	new vehicleID = mysql_insert_id(connectionHandle);
	PVeh[playerid][carslot][pvID] = vehicleID;
	return;
}


public LoadData_Vehicles(query[], playerid, extraid, connectionHandle) {
	new string[300], i=0, tmp[VEH_CHARACTERISTICS];
	mysql_store_result(connectionHandle);
	while(mysql_fetch_row_format(string, "|", connectionHandle)) {
		sscanf(string, "p<|>ie<s[32]iifiiii>", i, tmp);
		VehInfo[i-400] = tmp;
		if(tmp[V_C_PRICE] > 0) {
			SALE_VEHICLES++;
		}
	}
	mysql_free_result(connectionHandle);
	printf("[Vehicles] Loaded data of vehicles");
	printf("[Vehicles] %d vehicles are for sale", SALE_VEHICLES);
	return 1;
}

public LoadPlayerVehiclesInfo(query[], playerid, extraid, connectionHandle) {
	if(!LoggedIn(playerid)) 
		return 0;
	new rows=0, fields=0, string[128], bool:hasSpawnedCar=false;
	cache_get_data(rows, fields, connectionHandle);
	if(!rows) {
	    return 1;
	}
	for(new i=0;i<MAX_PLAYER_VEHICLES;i++) {
		new tmp[PVehInfo];
		PVeh[playerid][i] = tmp;
	}
	for(new i=0;i < rows;i++) {
		cache_get_field_content(i,  "id", string, connectionHandle);
		PVeh[playerid][i][pvID] = strval(string);
		cache_get_field_content(i,  "Model", string, connectionHandle);
		PVeh[playerid][i][pvModel]= strval(string);
		cache_get_field_content(i,  "Lock", string, connectionHandle);
		PVeh[playerid][i][pvLock]= strval(string);
		cache_get_field_content(i,  "Alarm", string, connectionHandle);
		PVeh[playerid][i][pvAlarm]= strval(string);
		cache_get_field_content(i,  "Immobilizer", string, connectionHandle);
		PVeh[playerid][i][pvImmob]= strval(string);
		cache_get_field_content(i,  "Insurance", string, connectionHandle);
		PVeh[playerid][i][pvInsurance]= strval(string);
		cache_get_field_content(i,  "Deaths", string, connectionHandle);
		PVeh[playerid][i][pvDeaths]= strval(string);
		if(!hasSpawnedCar) {
			foreach(new veh : Vehicle) {
				if(Vehicles[veh][vID] == PVeh[playerid][i][pvID] || Vehicles[veh][vOwner] == GetPVarInt(playerid, "ID")) {
					PVeh[playerid][i][pvCar] = veh;
					Vehicles[veh][vOwnerID] = playerid;
					SetPVarInt(playerid, "SpawnedVehicleSlot", i+1);
					hasSpawnedCar = true;
					break;
				}
			}
		}
	}
	return 1;
}

timer VehicleTimer[1000]() {
	foreach(new veh : Vehicle) {
		new Float:speed = GetVehicleSpeed(veh, false),
			Float:dist = speed / 3600,
			engine, lights, alarm, doors, bonnet, boot, objective;
		GetVehicleParamsEx(veh, engine, lights, alarm, doors, bonnet, boot, objective);
		if(!engine) {
			continue;
		}
		Vehicles[veh][vMileage] += dist;
		Vehicles[veh][vFuel] -= dist * VehInfo[GetVehicleModel(veh)-400][V_C_CONSUMPTION] + 0.002;
		if(Vehicles[veh][vFuel] < 0) {
			Vehicles[veh][vFuel] = 0;
			SetVehicleParamsEx(veh, !engine, lights, alarm, doors, bonnet, boot, objective);
			foreach(new ply : Player) {
				if(IsPlayerInVehicle(ply, veh) && GetPlayerState(ply) == PLAYER_STATE_DRIVER) {
					HookCommand(ply, "/do Engine has stopped");
					break;
				}
			}
		}
	}
	return 1;
}

hook OnGameModeInit() {
	Command_AddAltNamed("vehicle", "v");
	Command_AddAltNamed("engine", "en");
	mysql_query_callback(0, "SELECT `model`, `name`, (`type`-'1') as `type`, `tank`, `comsumption`, `wheels`, `weight`, `trunkSize`, `price` FROM `data_vehicles` ORDER BY `model` ASC ", "LoadData_Vehicles", 0, MySQL, false);
	mysql_query_callback(-1, "SELECT * FROM `vehicles` as veh, (SELECT `Name` FROM `factions` as f WHERE f.`id`= -`Owner`) as `OwnerName` WHERE `Owner` < '0'", "LoadVehicles", -1, MySQL, true);
	repeat VehicleTimer();
}

hook OnVehicleDamageStatusUpdate(vehicleid, playerid) {
	if(!Vehicles[vehicleid][vID]) 
			return;
	GetVehicleDamageStatus(vehicleid, Vehicles[vehicleid][vDamageStatus][0], 
		Vehicles[vehicleid][vDamageStatus][1], Vehicles[vehicleid][vDamageStatus][2], 
		Vehicles[vehicleid][vDamageStatus][3]);
}

hook OnGameModeExit() {
	printf("[Vehicles] Saving vehicles...");
	foreach(new i : Vehicle) {
		SaveVehicle(i);
	}
	printf("[Vehicles] Saving complete!");
	
}

hook OnPlayerStateChange(playerid, newstate, oldstate) {
	if(newstate == PLAYER_STATE_DRIVER) {
	}
}

hook OnPlayerConnect(playerid) {
	for(new i=0;i<MAX_PLAYER_VEHICLES;i++) {
		PVeh[playerid][i][pvID] = 0;
	}

	return 1;
}

hook OnPlayerDisconnect(playerid, reason) {
	foreach(Player, ply) {
		if(GetPVarInt(ply, "VehicleOfferedTo")-1 == playerid) {
			DeletePVar(ply, "VehicleOfferedTo");
			Text_Send(ply, $VEHICLE_BUYER_LEFT, GetPlayerNameEx(playerid));
		}
	}
	foreach(new veh : Vehicle) {
		if(Vehicles[veh][vOwnerID] == playerid) {
			Vehicles[veh][vOwnerID] = INVALID_PLAYER_ID;
		}
	}
	return 1;
}

hook OnVehicleMod(playerid, vehicleid, componentid) {
	Vehicles[vehicleid][vComponents][GetVehicleComponentType(componentid)] = componentid;
	return 1;
}

hook OnVehiclePaintjob(playerid, vehicleid, paintjobid) {
	Vehicles[vehicleid][vPaintJob] = paintjobid;
	return 1;	
}

hook OnVehicleRespray(playerid, vehicleid, color1, color2) {
	Vehicles[vehicleid][vColor1] = color1;
	Vehicles[vehicleid][vColor2] = color2;
	
	return 1;
}

hook OnVehicleDeath(vehicleid) {
	if(Vehicles[vehicleid][vID] == 0) 
		return 1;
	new string[128];
	Vehicles[vehicleid][vDeaths] ++;
	Vehicles[vehicleid][vInsurance] --;
	if(Vehicles[vehicleid][vInsurance] < 0) {
		Vehicles[vehicleid][vInsurance] = 0;
		Vehicles[vehicleid][vDisabled] = true;
		Vehicles[vehicleid][vHealth] = 500.0;
	}
	
	mysql_format(MySQL, string, "UPDATE `vehicles` SET `Insurance`='%d', `Deaths`='%d', `Disabled`='%d' WHERE `id`='%d'", 
		Vehicles[vehicleid][vInsurance],
		Vehicles[vehicleid][vDeaths],
		Vehicles[vehicleid][vDisabled],
		Vehicles[vehicleid][vID]);
	mysql_query(string, MySQL);
	if(Vehicles[vehicleid][vOwnerID] != INVALID_PLAYER_ID) {
		Text_Send(Vehicles[vehicleid][vOwnerID], $VEHICLE_GOT_DESTROYED, Vehicles[vehicleid][vInsurance]);
		ReloadPlayerVehicleList(Vehicles[vehicleid][vOwnerID]);
	}
	SetVehicleHealth(vehicleid, 1000.0);
	SaveVehicle(vehicleid);
	DestroyVehicle(vehicleid);
    return 1;
}

public PurchaseConfirmation(playerid, dialogid, response, listitem, string:inputtext[]) {
	#pragma unused inputtext, listitem, dialogid
	if(!response) {
		ShowVehiclePurchaseDialog(playerid, GetPVarInt(playerid, "DialogPage"));
		return 1;
	}
	new vehicleslot = GetPlayerFreeVehicleSlot(playerid), car = GetPVarInt(playerid, "VehicleToPurchase"), str[256];
	if(vehicleslot == -1) {
		Text_Send(playerid, $VEHICLE_LIMIT_REACHED);
		return 1;
	}
	if(GetPlayerMoney(playerid) <  VehInfo[car-400][V_C_PRICE]) {
		Text_Send(playerid, $PLAYER_CANT_AFFORD);
		return 1;
	}
	GivePlayerMoney(playerid, -VehInfo[car-400][V_C_PRICE]);
	format(str, sizeof(str), "INSERT INTO `vehicles` (`id`, `Owner`, `Model`, `Color1`, `Color2`, `CreatedTime` )  VALUES ( NULL, '%d', '%d', '%d', '%d', '%d' )",
		GetPVarInt(playerid, "ID"), car, random(127), random(127), gettime());
	mysql_query_callback(playerid, str, "CreateNewVehicle", vehicleslot, MySQL, true);
	PVeh[playerid][vehicleslot][pvID] = 0;
	PVeh[playerid][vehicleslot][pvCar] = 0;
	PVeh[playerid][vehicleslot][pvModel] = car;
	PVeh[playerid][vehicleslot][pvLock] = 0;
	PVeh[playerid][vehicleslot][pvImmob] = 0;
	PVeh[playerid][vehicleslot][pvAlarm] = 0;
	PVeh[playerid][vehicleslot][pvInsurance] = 2;
	PVeh[playerid][vehicleslot][pvDeaths] = 0;
	Text_Send(playerid, $VEHICLE_BOUGHT, VehInfo[car-400][V_C_NAME], VehInfo[car-400][V_C_PRICE]);
	return 1;
}

public PageDialog(playerid, dialogid, response, listitem, string:inputtext[]) {
	#pragma unused dialogid
	new playerPage = GetPVarInt(playerid, "DialogPage");
	if(!response) { 
		return 1;
	}
	if(listitem >= VEHICLES_PER_PAGE) {
		if(!strcmp(inputtext, "Next", true)) {
			ShowVehiclePurchaseDialog(playerid, playerPage+1);
			return 1;
		}
		else if(!strcmp(inputtext, "Previous", true)) {
			ShowVehiclePurchaseDialog(playerid, playerPage-1);
			return 1;
		}
	}
	new vehicleid, vehicleslot, str[56];
	sscanf(inputtext, "d", vehicleid);
	vehicleslot = GetPlayerFreeVehicleSlot(playerid);
	if(vehicleslot == -1) {
		Text_Send(playerid, $VEHICLE_LIMIT_REACHED);
		return 1;
	}
	SetPVarInt(playerid, "VehicleToPurchase", vehicleid);
	format(str, sizeof(str), "�� �������, ��� ������ ������ %s �� $%d?", VehInfo[vehicleid-400][V_C_NAME], VehInfo[vehicleid-400][V_C_PRICE]);
	Dialog_ShowCallback(playerid, using callback PurchaseConfirmation, DIALOG_STYLE_MSGBOX, "������� ����������", str, "������", "������");
	return 1;
}


YCMD:vehicle(playerid, params[], help) {
	if(help) 
		return Text_Send(playerid, $COMMAND_HELP, YCMD:veh, "vehicle management");
	new option[24], param1[48], param2[48], string[OUTPUT],
		veh = GetPlayerVehicleID(playerid), pID = GetPVarInt(playerid, "ID"),
		spawnedCar = GetSpawnedVehicleSlot(playerid);
	if(sscanf(params, "s[24]S()[48]S()[48]", option, param1, param2)) {
		Text_Send(playerid, $COMMAND_SYNTAX, YCMD:vehicle, "<arguments>");
		Text_Send(playerid, $COMMAND_ARGUMENTS, "list, get, park, sell, accept, register, lock, unregister");
		Text_Send(playerid, $COMMAND_ARGUMENTS, "buy, buylock, buyalarm, buyimmob, buyinsurance");
		Text_Send(playerid, $COMMAND_ARGUMENTS, "info, colour, faction, find");
		Text_Send(playerid, $COMMAND_ARGUMENTS, "scrap (removes vehicle completely!!!)");
		return 1;
	}
	if(!strcmp(option, "list", true)) {
		Text_Send(playerid, $VEHICLE_LIST);
		for(new i=0;i<MAX_PLAYER_VEHICLES;i++) {
			if(PVeh[playerid][i][pvID] == 0) continue;
			if(PVeh[playerid][i][pvCar] == 0 || i != GetSpawnedVehicleSlot(playerid)) 
				Text_Send(playerid, $VEHICLE_SHORT_INFO, i+1, VehInfo[PVeh[playerid][i][pvModel]-400][V_C_NAME], PVeh[playerid][i][pvLock], PVeh[playerid][i][pvAlarm], PVeh[playerid][i][pvImmob], PVeh[playerid][i][pvInsurance], PVeh[playerid][i][pvDeaths]);
			else 
				Text_Send(playerid, $VEHICLE_SHORT_INFO_ACTIVE, i+1, VehInfo[PVeh[playerid][i][pvModel]-400][V_C_NAME], PVeh[playerid][i][pvLock], PVeh[playerid][i][pvAlarm], PVeh[playerid][i][pvImmob], PVeh[playerid][i][pvInsurance], PVeh[playerid][i][pvDeaths], PVeh[playerid][i][pvCar]);
				
		}
		return 1;
	}
	else if(!strcmp(option, "get", true)) {
		if(isnull(param1)) {
			Text_Send(playerid, $COMMAND_SYNTAX, YCMD:vehicle, "get <vehicle id>");
			Text_Send(playerid, $VEHICLE_LIST_HELP, YCMD:vehicle);
			return 1;
		}
		if(spawnedCar != -1) {
			Text_Send(playerid, $VEHICLE_MUST_BE_PARKED);
			return 1;
		}
		new car = strval(param1)-1;
		if(car < 0 || car >= MAX_PLAYER_VEHICLES || !PVeh[playerid][car][pvID]) {
			Text_Send(playerid, $VEHICLE_NOT_EXISTS, car+1);
			return 1;
		}
		SetPVarInt(playerid, "SpawnedVehicleSlot", car+1);
		mysql_format(MySQL, string, "SELECT *, (SELECT `Name` from `characters` WHERE `id`=`vehicles`.`Owner`) as `OwnerName` FROM `vehicles` WHERE `id`='%d'", PVeh[playerid][car][pvID]);
		mysql_query_callback(playerid+1, string, "LoadVehicles", car, MySQL, true);
		
		return 1;
	}
	else if(!strcmp(option, "park", true)) {
		if(Vehicles[veh][vOwner] != pID)
			return Text_Send(playerid, $VEHICLE_NOT_YOURS);
		SaveVehicle(veh);
		Vehicles[veh][vOwner] = 0;
		Vehicles[veh][vID] = 0;
		PVeh[playerid][spawnedCar][pvCar] = 0;
		DeletePVar(playerid, "SpawnedVehicleSlot");
		DestroyVehicle(veh);
		Text_Send(playerid, $VEHICLE_GOT_PARKED, VehInfo[GetVehicleModel(veh)-400][V_C_NAME]);
		return 1;
	}
	else if(!strcmp(option, "sell", true)) {
		if(Vehicles[veh][vOwner] != pID)
			return Text_Send(playerid, $VEHICLE_NOT_YOURS);
		new ply = INVALID_PLAYER_ID, price = 0;
		if(sscanf(param1, "ui", ply, price) || strval(param2) < 1) {
			Text_Send(playerid, $COMMAND_SYNTAX, YCMD:vehicle, "sell <playerid> <price>");
			return 1;
		}
		if(!IsPlayerConnected(ply)) {
			Text_Send(playerid, $TARGET_NOT_LOGGED);
			return 1;
		}
		if(price < 1) {
			Text_Send(playerid, $MONEY_INVALID);
			return 1;
		}
		Text_Send(playerid, $VEHICLE_OFFER_SENT, VehInfo[GetVehicleModel(veh)-400][V_C_NAME], GetPlayerNameEx(ply), price);
		Text_Send(ply, $VEHICLE_OFFER_RECEIVED, VehInfo[GetVehicleModel(veh)-400][V_C_NAME], GetPlayerNameEx(playerid), price);
		SetPVarInt(playerid, "VehicleOfferedTo", ply+1);
		SetPVarInt(playerid, "VehicleOfferPrice", price);
		return 1;
	}
	else if(!strcmp(option, "accept", true)) {
		new ply = INVALID_PLAYER_ID, price = 0, Float:x, Float:y, Float:z,
			plySpawnedCar, carslot;
		if(sscanf(param1, "u", ply)) {
			Text_Send(playerid, $COMMAND_SYNTAX, YCMD:vehicle, "accept <playerid>");
			return 1;
		}
		if(ply == playerid) {
			Text_Send(playerid, $PLAYER_CMD_SELF);
			return 1;
		}
		if(!IsPlayerConnected(ply)) {
			Text_Send(playerid, $TARGET_NOT_LOGGED);
			return 1;
		}
		if(GetPVarInt(ply, "VehicleOfferedTo")-1 != playerid) {
			Text_Send(playerid, $VEHICLE_NOT_OFFERED);
			return 1;
		}
		price = GetPVarInt(ply, "VehicleOfferPrice");
		if(GetPlayerMoney(playerid) < price) {
			Text_Send(playerid, $PLAYER_CANT_AFFORD);
			return 1;
		}

		GetPlayerPos(playerid, x, y, z);
		if(!IsPlayerInRangeOfPoint(ply, 10.0, x, y, z)) {
			Text_Send(playerid, $PLAYER_IS_TOO_FAR);
			return 1;
		}
		if(spawnedCar != -1) {
			Text_Send(playerid, $VEHICLE_MUST_BE_PARKED);
			return 1;
		}
		carslot = GetPlayerFreeVehicleSlot(playerid);
		if(carslot == -1) {				
			Text_Send(playerid, $VEHICLE_LIMIT_REACHED);
			return 1;
		}
		plySpawnedCar = GetSpawnedVehicleSlot(ply);
		GivePlayerMoney(playerid, -price);
		GivePlayerMoney(ply, price);
		Text_Send(ply, $VEHICLE_OFFER_ACCEPTED, GetPlayerNameEx(playerid));
		Text_Send(playerid, $VEHICLE_OFFER_ACCEPTED_SELF, GetPlayerNameEx(ply));
		
		PVeh[playerid][carslot] = PVeh[ply][plySpawnedCar];
		PVeh[ply][plySpawnedCar][pvID] = 0;
		
		GetPlayerName(ply, Vehicles[PVeh[playerid][carslot][pvCar]][vOwnerName], MAX_PLAYER_NAME);
		
		SortPlayerVehicles(ply);
		
		DeletePVar(ply, "SpawnedVehicleSlot");
		
		format(string, sizeof(string), "UPDATE `vehicles` SET `Owner` = '%d' WHERE `id` = '%d'", playerid, PVeh[playerid][carslot][pvID]);
		mysql_query(string, MySQL);
		return 1;
	}
	else if(!strcmp(option, "cancel", true)) {
		new ply = INVALID_PLAYER_ID;
		if(sscanf(param1, "u", ply)) {
			Text_Send(playerid, $COMMAND_SYNTAX, YCMD:vehicle, "cancel <playerid>");
			return 1;
		}
		if(ply == playerid) {
			Text_Send(playerid, $PLAYER_CMD_SELF);
			return 1;
		}
		if(!IsPlayerConnected(ply)) {
			Text_Send(playerid, $TARGET_NOT_LOGGED);
			return 1;
		}
		if(GetPVarInt(ply, "VehicleOfferedTo")-1 != playerid) {
			Text_Send(playerid, $VEHICLE_NOT_OFFERED);
			return 1;
		}
		Text_Send(ply, $VEHICLE_OFFER_CANCELLED, GetPlayerNameEx(playerid));
		Text_Send(playerid, $VEHICLE_OFFER_CANCELLED_SELF, GetPlayerNameEx(ply));
		DeletePVar(ply, "SpawnedVehicleSlot");
		return 1;
	}
	else if(!strcmp(option, "register", true)) {
		if(Vehicles[veh][vOwner] != pID)
			return Text_Send(playerid, $VEHICLE_NOT_YOURS);
		new numberPlate[12];
		if(!isnull(param1)) {
			if(!Group_GetPlayer(gPremium[Gold], playerid)) {
				return Text_Send(playerid, $PREMIUM_LEVEL_GOLD);
			}
			strmid(numberPlate, param1, 0, sizeof(numberPlate));
		}
		else {
			sscanf(GetPlayerNameEx(playerid), "s[12]s[12]", param1, param2);
			format(numberPlate, sizeof(numberPlate), "%c%c%d", param1[0], param2[0], 100000+Vehicles[veh][vID]%100000);
		}
		strmid(Vehicles[veh][vPlate], numberPlate, 0, strlen(numberPlate));
		SetVehicleNumberPlate(veh, numberPlate);
		Text_Send(playerid, $VEHICLE_REGISTERED, VehInfo[GetVehicleModel(veh)-400][V_C_NAME], numberPlate);
		mysql_format(MySQL, string, "UPDATE `vehicles` SET `Plate`='%s' WHERE `id`='%d'", numberPlate, Vehicles[veh][vID]);
		mysql_query(string, MySQL);
		return 1;
	}
	else if(!strcmp(option, "alarm", true)) {
		// debug command
		// todo: remove it before the release
		new engine, lights, alarm, doors, bonnet, boot, objective;
		GetVehicleParamsEx(veh, engine, lights, alarm, doors, bonnet, boot, objective);
		SetVehicleParamsEx(veh, engine, lights, !alarm, doors, bonnet, boot, objective);
		return 1;
	}
	else if(!strcmp(option, "lock", true)) {
		new closestCar = GetClosestVehicle(playerid, 10.0), engine, lights, alarm, doors, bonnet, boot, objective;
		if(closestCar == 0) {
			Text_Send(playerid, $VEHICLE_NOT_NEAR);
			return 1;
		}
		if(Vehicles[closestCar][vLock] < 1) {
			Text_Send(playerid, $VEHICLE_HAS_NO_LOCK);
			return 1;
		}
		if(Vehicles[closestCar][vOwner] != playerid && !(Vehicles[closestCar][vFaction] == GetPVarInt(playerid, "Org") &&
			Vehicles[closestCar][vFaction] != 0)) {
			if(itemValues:Items[GetPVarInt(playerid, EquipSlots[1][0])][itemValue] == ITEM_VALUE_LOCKPICK) {
				Player_InitializeCountdown(playerid, VehicleBreakoutTime[Vehicles[closestCar][vLock]-1], "BreakInVehicle"); 
				HookCommand(playerid, "/ame takes out the lockpick and starts breaking the lock");
				SetPVarInt(playerid, "PlayerBreakingVehicle", closestCar);
				SetPVarInt(playerid, "PlayerBreakingVehicleType", 1);
				return 1;
			}
			Text_Send(playerid, $VEHICLE_NO_KEYS);
			return 1;
		}
		GetVehicleParamsEx(closestCar, engine, lights, alarm, doors, bonnet, boot, objective);
		SetVehicleParamsEx(closestCar, engine, lights, alarm, !doors, bonnet, boot, objective);
		if(doors) {
			Text_Send(playerid, $VEHICLE_GOT_UNLOCKED, VehInfo[GetVehicleModel(closestCar)-400][V_C_NAME]);
		}
		else {
			Text_Send(playerid, $VEHICLE_GOT_LOCKED, VehInfo[GetVehicleModel(closestCar)-400][V_C_NAME]);
		}
		return 1;
	}
	else if(!strcmp(option, "unregister", true)) {	
		if(Vehicles[veh][vOwner] != pID)
			return Text_Send(playerid, $VEHICLE_NOT_YOURS);		
		if(!Group_GetPlayer(gPremium[Gold], playerid)) {
			return Text_Send(playerid, $PREMIUM_LEVEL_BRONZE);
		}
		strmid(Vehicles[veh][vPlate], "", 0, 12);
		SetVehicleNumberPlate(veh, "");
		Text_Send(playerid, $VEHICLE_UNREGISTERED, VehInfo[GetVehicleModel(veh)-400][V_C_NAME]);
		mysql_format(MySQL, string, "UPDATE `vehicles` SET `Plate`=' ' WHERE `id`='%d'", Vehicles[veh][vID]);
		mysql_query(string, MySQL);
		return 1;
	}
	else if(!strcmp(option, "buy", true)) {
		ShowVehiclePurchaseDialog(playerid, 1);
		return 1;
	}
	else if(!strcmp(option, "buylock", true)) {
		if(isnull(param1)) {
			Text_Send(playerid, $COMMAND_SYNTAX, YCMD:vehicle, "buylock <level>");
			return 1;
		}
		if(Vehicles[veh][vOwner] != pID)
			return Text_Send(playerid, $VEHICLE_NOT_YOURS);	
		new level = strval(param1);
		if(level < 1 || level > 5) 
			return Text_Send(playerid, $VEHICLE_UP_LEVEL, 1, 5);
		if(Vehicles[veh][vLock] > level)
			return Text_Send(playerid, $VEHICLE_UPGRADE_INSTALLED);
		Vehicles[veh][vLock] = level;
		PVeh[playerid][spawnedCar][pvLock] = level;
		Text_Send(playerid, $VEHICLE_LOCK_INSTALLED, level);
		mysql_format(MySQL, string, "UPDATE `vehicles` SET `Lock`='%d' WHERE `id`='%d'", level, Vehicles[veh][vID]);
		mysql_query(string, MySQL);
		return 1;
	}
	else if(!strcmp(option, "buyalarm", true)) {
		if(isnull(param1)) {
			Text_Send(playerid, $COMMAND_SYNTAX, YCMD:vehicle, "buyalarm <level>");
			return 1;
		}
		if(Vehicles[veh][vOwner] != pID)
			return Text_Send(playerid, $VEHICLE_NOT_YOURS);	
		new level = strval(param1);
		if(level < 1 || level > 4) 
			return Text_Send(playerid, $VEHICLE_UP_LEVEL, 1, 4);
		if(Vehicles[veh][vAlarm] > level)
			return Text_Send(playerid, $VEHICLE_UPGRADE_INSTALLED);
		Vehicles[veh][vAlarm] = level;
		PVeh[playerid][spawnedCar][pvAlarm]++;
		Text_Send(playerid, $VEHICLE_ALARM_INSTALLED, level);
		mysql_format(MySQL, string, "UPDATE `vehicles` SET `Alarm`='%d' WHERE `id`='%d'", level, Vehicles[veh][vID]);
		mysql_query(string, MySQL);
		return 1;
	}
	else if(!strcmp(option, "buyinsurance", true) || !strcmp(option, "buyins", true)) {
		if(Vehicles[veh][vOwner] != pID)
			return Text_Send(playerid, $VEHICLE_NOT_YOURS);	
		Vehicles[veh][vInsurance]++;
		PVeh[playerid][spawnedCar][pvInsurance]++;
		Text_Send(playerid, $VEHICLE_INSURANCE_BOUGHT, 0);
		mysql_format(MySQL, string, "UPDATE `vehicles` SET `Insurance`=`Insurance`+1 WHERE `id`='%d'", Vehicles[veh][vID]);
		mysql_query(string, MySQL);
		return 1;
		
	}
	else if(!strcmp(option, "buyimmob", true)) {
		if(isnull(param1)) {
			Text_Send(playerid, $COMMAND_SYNTAX, YCMD:vehicle, "buyimmob <level>");
			return 1;
		}
		if(Vehicles[veh][vOwner] != pID)
			return Text_Send(playerid, $VEHICLE_NOT_YOURS);	
		new level = strval(param1);
		if(level < 1 || level > 4) 
			return Text_Send(playerid, $VEHICLE_UP_LEVEL, 1, 4);
		if(Vehicles[veh][vImmob] > level)
			return Text_Send(playerid, $VEHICLE_UPGRADE_INSTALLED);
		Vehicles[veh][vImmob] = level;
		PVeh[playerid][spawnedCar][pvImmob] = level;
		Text_Send(playerid, $VEHICLE_IMMOB_INSTALLED, level);
		mysql_format(MySQL, string, "UPDATE `vehicles` SET `Immobilizer`='%d' WHERE `id`='%d'", level, Vehicles[veh][vID]);
		mysql_query(string, MySQL);
		return 1;
	}
	else if(!strcmp(option, "info", true)) {
		if(Vehicles[veh][vOwner] != pID)
			return Text_Send(playerid, $VEHICLE_NOT_YOURS);
		if(spawnedCar != -1)
			Text_Send(playerid, $VEHICLE_SHORT_INFO_ACTIVE, spawnedCar+1, VehInfo[GetVehicleModel(veh)-400][V_C_NAME], Vehicles[veh][vLock], Vehicles[veh][vAlarm], Vehicles[veh][vImmob], Vehicles[veh][vInsurance], Vehicles[veh][vDeaths], veh);
		else
			Text_Send(playerid, $VEHICLES_NOT_SPAWNED);
		return 1;
	}
	else if(!strcmp(option, "colour", true)) {
		if(isnull(param1) || isnull(param2)) {
			Text_Send(playerid, $COMMAND_SYNTAX, YCMD:vehicle, "colour <0-126> <0-126>");
			return 1;
		}
		new color1 = strval(param1), color2 = strval(param2);
		if((color1 < 0 || color1 > 126 || color2 < 0 || color2 > 126) && !Group_GetPlayer(gPremium[Bronze], playerid)) {
			Text_Send(playerid, $VEHICLE_COLOR_INVALID);
			return 1;
		}
		if(Vehicles[veh][vOwner] != pID)
			return Text_Send(playerid, $VEHICLE_NOT_YOURS);
		ChangeVehicleColor(veh, color1, color2);
		Vehicles[veh][vColor1] = color1;
		Vehicles[veh][vColor2] = color2;
		Text_Send(playerid, $VEHICLE_REPAINTED);
		mysql_format(MySQL, string, "UPDATE `vehicles` SET `Color1`='%d', `Color2`='%d' WHERE `id`='%d'", color1, color2, Vehicles[veh][vID]);
		mysql_query(string, MySQL);
		return 1;
	}
	else if(!strcmp(option, "faction", true)) {
		new pOrg = GetPVarInt(playerid, "Org"); 
		if(pOrg == 0) {
			Text_Send(playerid, $NOT_A_MEMBER);
			return 1;
		}
		if(Vehicles[veh][vOwner] != pID)
			return Text_Send(playerid, $VEHICLE_NOT_YOURS);
		if(!strcmp(param1, "true", true)) {
			Text_Send(playerid, $VEHICLE_LINKED);
			Vehicles[veh][vFaction] = pOrg;
			mysql_format(MySQL, string, "UPDATE `vehicles` SET `Faction`='%d' WHERE `id`='%d'", pOrg, Vehicles[veh][vID]);
			mysql_query(string, MySQL);
			return 1;
		}
		else if(!strcmp(param1, "false", true)) {
			Text_Send(playerid, $VEHICLE_UNLINKED);
			Vehicles[veh][vFaction] = 0;
			mysql_format(MySQL, string, "UPDATE `vehicles` SET `Faction`='0' WHERE `id`='%d'", Vehicles[veh][vID]);
			mysql_query(string, MySQL);
			return 1;
		}
		Text_Send(playerid, $COMMAND_SYNTAX, YCMD:vehicle, "faction <true/false>");
		return 1;
	}
	else if(!strcmp(option, "find", true)) {
		new Float:x, Float:y, Float:z;
		if(spawnedCar == -1)  {
			Text_Send(playerid, $VEHICLES_NOT_SPAWNED);
			return 1;
		}
		foreach(Player, ply) {
			if(IsPlayerInVehicle(ply, PVeh[playerid][spawnedCar][pvCar])) {
				Text_Send(playerid, $VEHICLE_OCCUPIED);
				return 1;
			}
		}
		GetVehiclePos(PVeh[playerid][spawnedCar][pvCar], x, y, z);	
		SetPlayerCheckpoint(playerid, x, y, z, 10.0, 1);
		Text_Send(playerid, $VEHICLE_FOUND);
		return 1;
	}
#if defined vehicle_inventory_included
	else if(!strcmp(option, "inv", true)) {
		new engine, lights, alarm, doors, bonnet, boot, objective;
		veh = GetClosestVehicle(playerid, 5.0);
		if(IsPlayerInAnyVehicle(playerid)) {
			Text_Send(playerid, $STATE_ONFOOT);
			return 1;
		}
		if(veh == 0) {
			Text_Send(playerid, $VEHICLE_NOT_NEAR);
			return 1;
		}
		if(Vehicles[veh][vOwner] != pID && Vehicles[veh][vFaction] != GetPVarInt(playerid, "Org") &&
			Vehicles[veh][vFaction] != 0) {
			Text_Send(playerid, $VEHICLE_NO_KEYS);
			return 1;
		}
		GetVehicleParamsEx(veh, engine, lights, alarm, doors, bonnet, boot, objective);
		if(!boot) {
			if(veh == 0) {
				Text_Send(playerid, $VEHICLE_NOT_NEAR);
				return 1;
			}
			if(Vehicles[veh][vOwner] != pID && Vehicles[veh][vFaction] != GetPVarInt(playerid, "Org") &&
				Vehicles[veh][vFaction] != 0) {
				Text_Send(playerid, $VEHICLE_NO_KEYS);
				return 1;
			}
		}
		Player_ShowVehicleInventory(playerid, veh);
		return 1;
	}
#endif
	else {
		HookCommand(playerid, "/vehicle");
	}
	return 1;
}

YCMD:engine(playerid, params[], help) {
	if(help) 
		return Text_Send(playerid, $COMMAND_HELP, YCMD:veh, "controlling vehicle engine");
	new string[OUTPUT],
		veh = GetPlayerVehicleID(playerid), pID = GetPVarInt(playerid, "ID"),
		engine, lights, alarm, doors, bonnet, boot, objective;
	if(veh == 0 || veh == INVALID_VEHICLE_ID) {
		return 1;
	}
	if(Vehicles[veh][vOwner] != pID && !(Vehicles[veh][vFaction] == GetPVarInt(playerid, "Org") &&
		Vehicles[veh][vFaction] != 0)) {
		if(itemValues:Items[GetPVarInt(playerid, EquipSlots[1][0])][itemValue] == ITEM_VALUE_LOCKPICK) {
			Player_InitializeCountdown(playerid, VehicleBreakoutTime[Vehicles[veh][vLock]-1], "BreakInVehicle"); 
			HookCommand(playerid, "/me attempts to hotwire the vehicle");
			SetPVarInt(playerid, "PlayerBreakingVehicle", veh);
			SetPVarInt(playerid, "PlayerBreakingVehicleType", 2);
			return 1;
		}
		Text_Send(playerid, $VEHICLE_NO_KEYS);
		return 1;
	}
	GetVehicleParamsEx(veh, engine, lights, alarm, doors, bonnet, boot, objective);
	if(engine) {
		format(string, sizeof(string), "/me stops the engine of  %s", VehInfo[Vehicles[veh][vModel]-400][V_C_NAME]);
		HookCommand(playerid, string);
	}
	else {
		if(Vehicles[veh][vDisabled]) {
			Text_Send(playerid, $VEHICLE_IS_DISABLED);
			return 1;
		}
		GetVehicleHealth(veh, Vehicles[veh][vHealth]);
		if(Vehicles[veh][vHealth] < 700.0 && random(3000/floatround(Vehicles[veh][vHealth]))) { 
			HookCommand(playerid, "/ame attempts to start the engine, but failed");
			return 1;
		}
		format(string, sizeof(string), "/me starts the engine of %s", VehInfo[Vehicles[veh][vModel]-400][V_C_NAME]);
		HookCommand(playerid, string);
	}
	
	SetVehicleParamsEx(veh, !engine, lights, alarm, doors, bonnet, boot, objective);
	return 1;
}

YCMD:lights(playerid, params[], help) {
	if(help) 
		return Text_Send(playerid, $COMMAND_HELP, YCMD:veh, "turning vehicle lights on and off");
	new veh = GetPlayerVehicleID(playerid),
		engine, lights, alarm, doors, bonnet, boot, objective;
		
	GetVehicleParamsEx(veh, engine, lights, alarm, doors, bonnet, boot, objective);
	SetVehicleParamsEx(veh, engine, !lights, alarm, doors, bonnet, boot, objective);
	return 1;
}

YCMD:boot(playerid, params[], help) {
	new engine, lights, alarm, doors, bonnet, boot, objective,
		pID = GetPVarInt(playerid, "ID"), veh = GetPlayerVehicleID(playerid);
	if(GetPlayerState(playerid) != PLAYER_STATE_DRIVER) {
		veh = GetClosestVehicle(playerid, 5.0);
		if(IsPlayerInAnyVehicle(playerid)) {
			return 1;
		}
		if(veh == 0) {
			Text_Send(playerid, $VEHICLE_NOT_NEAR);
			return 1;
		}
		if(Vehicles[veh][vOwner] != pID && !(Vehicles[veh][vFaction] == GetPVarInt(playerid, "Org") &&
			Vehicles[veh][vFaction] != 0)) {
			Text_Send(playerid, $VEHICLE_NO_KEYS);
			return 1;
		}
	}
	GetVehicleParamsEx(veh, engine, lights, alarm, doors, bonnet, boot, objective);
	SetVehicleParamsEx(veh, engine, lights, alarm, doors, bonnet, !boot, objective);
	return 1;
}

YCMD:bonnet(playerid, params[], help) {
	new engine, lights, alarm, doors, bonnet, boot, objective,
		pID = GetPVarInt(playerid, "ID"), veh = GetPlayerVehicleID(playerid);
	if(GetPlayerState(playerid) != PLAYER_STATE_DRIVER) {
		veh = GetClosestVehicle(playerid, 5.0);
		if(IsPlayerInAnyVehicle(playerid)) {
			return 1;
		}
		if(veh == 0) {
			Text_Send(playerid, $VEHICLE_NOT_NEAR);
			return 1;
		}
		if(Vehicles[veh][vOwner] != pID && !(Vehicles[veh][vFaction] == GetPVarInt(playerid, "Org") &&
			Vehicles[veh][vFaction] != 0)) {
			Text_Send(playerid, $VEHICLE_NO_KEYS);
			return 1;
		}
	}
	GetVehicleParamsEx(veh, engine, lights, alarm, doors, bonnet, boot, objective);
	SetVehicleParamsEx(veh, engine, lights, alarm, doors, !bonnet, boot, objective);
	return 1;
}

public BreakInVehicle(playerid, bool:cancelled) {
	if(cancelled) {
		GameTextForPlayer(playerid, "~r~CANCELLED", 2000, 5);
		DeletePVar(playerid, "PlayerBreakingVehicle");
		DeletePVar(playerid, "PlayerBreakingVehicleType");
		return 1;
	}
	new veh = GetPVarInt(playerid, "PlayerBreakingVehicle"), engine, lights, alarm, doors, bonnet, boot, objective;
	GetVehicleParamsEx(veh, engine, lights, alarm, doors, bonnet, boot, objective);
	if(GetPVarInt(playerid, "PlayerBreakingVehicleType") == 1) { // lock
		if(PlayerToVehicle(playerid, veh) > 10.0 || !Vehicles[veh][vModel]) {
			return 1;
		}
		Text_Send(playerid, $VEHICLE_GOT_UNLOCKED, VehInfo[GetVehicleModel(veh)-400][V_C_NAME]);
		SetVehicleParamsEx(veh, engine, lights, alarm, !doors, bonnet, boot, objective);
	}
	else {
		if(!IsPlayerInVehicle(playerid, veh) || GetPlayerState(playerid) != PLAYER_STATE_DRIVER) {
			Text_Send(playerid, $STATE_DRIVER);
			return 1;
		}
		new string[OUTPUT];
		format(string, sizeof(string), "/me starts the engine of %s", VehInfo[Vehicles[veh][vModel]-400][V_C_NAME]);
		HookCommand(playerid, string);
		SetVehicleParamsEx(veh, !engine, lights, alarm, doors, bonnet, boot, objective);
	}
	DeletePVar(playerid, "PlayerBreakingVehicle");
	DeletePVar(playerid, "PlayerBreakingVehicleType");
	return 1;
}