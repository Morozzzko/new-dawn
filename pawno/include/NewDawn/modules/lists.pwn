
stock CheckLists(playerid) {
	CheckBlackList(playerid);
    return;
}

stock CheckWhiteList(playerid) {
    new string[200];
    mysql_format(MySQL, string, "SELECT `id` FROM `whitelist` WHERE `PlayerID`='%d' LIMIT 1;", GetPVarInt(playerid, "PlayerID"));
    mysql_query_callback(-1, string, "CheckWhiteListCallback", playerid, MySQL, true);
	return;
}

forward CheckWhiteListCallback(query[], unused, extraid, connectionHandle);
public CheckWhiteListCallback(query[], unused, extraid, connectionHandle) { 
    new rows=0, fields=0;
    cache_get_data(rows, fields, connectionHandle);
    if(rows <= 0) {
		Kick(extraid);
	}
    return 1;
}

stock CheckBlackList(playerid) {
    new string[200], pIP[24];
    GetPlayerIp(playerid, pIP, sizeof(pIP));
    mysql_format(MySQL, string, "SELECT `id` FROM `blacklist` WHERE '%s' LIKE CONCAT(`subnetwork`,'\%') LIMIT 1;", pIP);
    mysql_query_callback(-1, string, "CheckBlackListCallback", playerid, MySQL, true);
    return;
}

forward CheckBlackListCallback(query[], unused, extraid, connectionHandle);
public CheckBlackListCallback(query[], unused, extraid, connectionHandle) {
    new rows=0, fields=0;
    cache_get_data(rows, fields, connectionHandle);
    if(rows > 0) {
		CheckWhiteList(extraid);
	}
    return 1;
}

/**
 * Bans a specific player
 *  
 * @param int ID of the player to ban
 * @param int number of minutes to ban for. 0 for permanent ban
 * @param string reason of ban
 */
stock BanPlayer(playerid, time, const string:reason[]) {
	
}