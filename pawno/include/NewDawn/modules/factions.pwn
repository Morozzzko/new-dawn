/**
 * factions.pwn
 *
 * Source code file for faction system
 * @author Moroz <moroz@malefics.com>
 * @version 1.0
 *
 * Commands:
 * @see YCMD:faction
 *
 */

#if !defined factions_included
	#endinput
#endif

#include <YSI\y_text>


loadtext Factions[Messages], Factions[Errors], Core[Commands], Core[Errors];
 


/**
 * Counts faction players online
 *
 * @param int faction id
 * @return number of players online
 */

stock CountFactionMembersOnline(factionid) {
	if(factionid < 0 || factionid >= MAX_FACTIONS) return 0;
	new count = 0;
	factionid++;
	foreach(new playerid: Player) {
		if(GetPlayerFaction(playerid) == factionid) {
			count ++;
		}
	}
	return count;
}


/**
 * Returns player's faction number (1..MAX_FACTION)
 *
 * @param int player id
 */
stock GetPlayerFaction(playerid) {
	return GetPVarInt(playerid, "Org");
}


/**
 * Returns a player's current faction type. 
 *
 * @param int player id
 */
stock GetPlayerFactionType(playerid) {
	new faction = GetPlayerFaction(playerid)-1;
	if(faction == -1) {
		return FACTION_TYPE_NONE;
	}
	return FactionInfo[faction][fType];
}




/**
 * Returns player's rank number
 *
 * @param int player id
 */
stock GetPlayerRank(playerid) {
	new rank = GetPVarInt(playerid, "Org");
	return rank > 0 ? (rank <= MAX_RANK) ? rank : MAX_RANK : 1;
}


/**
 * Returns a player's current rank access. 
 *
 * @param int player id
 */
stock GetPlayerAccessLevel(playerid) {
	new faction = GetPlayerFaction(playerid)-1, 
		rank = GetPlayerRank(playerid);
	if(faction == -1) {
		return 0;
	}
	return FactionRankAccess[faction][rank];
}

/**
 * Name says for itself.
 * @param int faction ID (0..MAX_FACTION-1)
 * @return int 1 on success, 0 if invalid faction specified
 */
stock SaveFaction(factionid) {
	if(factionid < 0 || factionid >= MAX_FACTIONS) {
		return 0;
	}
	new query[512], buff[32];
	mysql_real_escape_string(FactionInfo[factionid][fName], query, MySQL);
	format(query, sizeof(query), "UPDATE `factions` SET `Bank`='%d', `RankNames`='", query, FactionInfo[factionid][fBank]);
	for(new i=0;i<MAX_RANK;i++) {
		mysql_real_escape_string(FactionRanks[factionid][i], buff, MySQL);
		strcat(query, buff); 
		strcat(query, ";");
	}
	strmid(query, query, 0, strlen(query)-1);
	strcat(query, "', `RankPaycheck`='");
	for(new i=0;i<MAX_RANK;i++) {
		strcat(query, FactionRankPaycheck[factionid][i]);
		strcat(query, ";");
	}
	strmid(query, query, 0, strlen(query)-1);
	strcat(query, "', `RankAccess`='");
	for(new i=0;i<MAX_RANK;i++) {
		format(query, sizeof(query), "%s%d;", query, FactionRankAccess[factionid][i]);
	}
	strmid(query, query, 0, strlen(query)-1);
	format(query, sizeof(query), "%s' WHERE `id`='%d'", query, factionid+1);
	mysql_query(query, MySQL);
	return 1;
}


public LoadFactions(query[], index, extraid, connectionHandle) {
	new rows=0, fields=0;
	cache_get_data(rows, fields, connectionHandle);
	if(!rows) {
		printf("[Factions] No factions loaded: no faction found.");
	    return 1;
	}
    new string[256], i=0;
	for(new k=0;k < rows;k++) {
		cache_get_field_content(k,  "id", string, connectionHandle);
		
		i = strval(string)-1;
		if(i < 0 || i >= MAX_FACTIONS) continue;
		cache_get_field_content(k,  "Name", FactionInfo[i][fName], connectionHandle);
		cache_get_field_content(k, "Owner", string, connectionHandle);
		FactionInfo[i][fOwner] = strval(string);
		cache_get_field_content(k, "OwnerName", FactionInfo[i][fOwnerName], connectionHandle);
		
		cache_get_field_content(k, "Type", string, connectionHandle);
		FactionInfo[i][fType] = strval(string);
		
		cache_get_field_content(k, "Bank", string, connectionHandle);
		FactionInfo[i][fBank] = strval(string);
		
		cache_get_field_content(k, "Members", string, connectionHandle);
		FactionInfo[i][fMembers] = strval(string);
		
		cache_get_field_content(k, "RankNames", string, connectionHandle);
		split(string,  FactionRanks[i], ';');
		
		cache_get_field_content(k, "RankPaycheck", string, connectionHandle);
		sscanf(string, "p<;>A<i>(0)[15]", FactionRankPaycheck[i]);
		
		cache_get_field_content(k, "RankAccess", string, connectionHandle);
		sscanf(string, "p<;>A<i>(0)[15]", FactionRankAccess[i]);
		
		FactionInfo[i][fGroup] = Group_Create(FactionInfo[i][fName]);
		printf("[Factions] Loaded faction #%d: %s (Owner: %s [SID:%d])", i+1, FactionInfo[i][fName], FactionInfo[i][fOwnerName], FactionInfo[i][fOwner]);
	
	}
	printf("[Factions] %d factions have been loaded.", rows);
	return 1;
}

hook OnGameModeInit() {
	new string[256];
	format(string, sizeof(string), "SELECT *, (SELECT COUNT(*) AS `Members` FROM `characters` WHERE `Org`=`factions`.`id`) AS `Members`, (SELECT `Name` from `characters` WHERE `id`=`factions`.`Owner`) AS `OwnerName`  FROM `factions` LIMIT %d", MAX_FACTIONS);
	for(new i=0;i<MAX_FACTIONS;i++)
		FactionInfo[i][fOwner] = 0;
	
	mysql_query_callback(0, string,"LoadFactions",-1, MySQL, true);

}


new FACTION_HELP_TEXT[] = "This is the faction menu. \nHere you will see information about factions, such as how many players it has, \nthe rank structure, and the amount of money in the faction bank.";

forward FactionDialog_Rank_Value(playerid, dialogid, response, listitem, string:inputtext[]);
public FactionDialog_Rank_Value(playerid, dialogid, response, listitem, string:inputtext[]) {
	new pFaction = GetPlayerFaction(playerid)-1,
		rank = GetPVarInt(playerid, "EditedRank");
	listitem = GetPVarInt(playerid, "EditedRankOption");
	if(!response) {
		FactionDialog_Ranks(playerid, 0, 1, rank, "");
		return 1;
	}
	if(listitem == 0) { // name
		strmid(FactionRanks[pFaction][rank], inputtext, 0, 16);
		SaveFaction(pFaction);
	}
	else if(listitem == 1) { // salary
		new value = strval(inputtext);
		if(value < 0) {
			FactionDialog_Rank_Edition(playerid, dialogid, response, listitem, "");
			return 1;
		}
		FactionRankPaycheck[pFaction][rank] = value;
		SaveFaction(pFaction);
	} 
	else if(listitem == 2) { // access
		new value = strval(inputtext);
		if(value < 0 || value > 10) {
			FactionDialog_Rank_Edition(playerid, dialogid, response, listitem, "");
			return 1;
		}
		FactionRankAccess[pFaction][rank] = value;
		SaveFaction(pFaction);
	}
	FactionDialog_Ranks(playerid, 0, response, rank, "");
	return 1;
}

forward FactionDialog_Rank_Edition(playerid, dialogid, response, listitem, string:inputtext[]);
public FactionDialog_Rank_Edition(playerid, dialogid, response, listitem, string:inputtext[]) {
	new pFaction = GetPlayerFaction(playerid)-1,
		string[1024], 
		title[128],
		rank = GetPVarInt(playerid, "EditedRank");
	if(!response) {
		FactionDialog_Main(playerid, dialogid, 1, listitem, "Faction ranks");
		return 1;
	}
	format(title, sizeof(title), "{00BB00}[Faction] {FFFFFF}%s - rank #%d", FactionInfo[pFaction][fName], rank+1);
	if(listitem == 0) { // title
		format(string, sizeof(string), "{FFFFFF}Enter the new {33CCFF}title{FFFFFF} of the rank.\n\
			The current title is {33CCFF}%s{FFFFFF}.",
			FactionRanks[pFaction][rank]
		); 
	}
	else if(listitem == 1) { // salary
		format(string, sizeof(string), "{FFFFFF}Enter the new value of {33CCFF}salary{FFFFFF}.\n\
			The current value is %d.",
			FactionRankPaycheck[pFaction][rank]
		); 
	}
	else if(listitem == 2) { // access level
		format(string, sizeof(string), "{FFFFFF}Enter the new value of {33CCFF}access level{FFFFFF}.\n\
			The current value is %d. Possible range is 0-10.",
			FactionRankAccess[pFaction][rank]
		); 
	}
	SetPVarInt(playerid, "EditedRankOption", listitem);
	Dialog_ShowCallback(playerid, using callback FactionDialog_Rank_Value, DIALOG_STYLE_INPUT, title, string, "Edit", "Back");
	return 1;
}
forward FactionDialog_Dummy(playerid, dialogid, response, listitem, string:inputtext[]);
public FactionDialog_Dummy(playerid, dialogid, response, listitem, string:inputtext[]) {
	Command_ReProcess(playerid, "/faction", 0);
	return 1;
}



forward FactionDialog_KickPlayer(playerid, response, ply);
public FactionDialog_KickPlayer(playerid,  response, ply) {
	if(!response || !LoggedIn(ply)) {
		FactionDialog_Members(playerid, 0, 1, 0, GetPlayerNameEx(ply, false));
		return 1;
	}
	SetPVarInt(ply, "Org", 0);
	SetPVarInt(ply, "Rank", 0);
	Text_Send(ply, $FACTION_LOG_KICKED, playerid, ply);
	Text_Send(playerid, $FACTION_LOG_KICKED, playerid, ply);
	return 1;
}

forward FactionDialog_Member_Edit(playerid, dialogid, response, listitem, string:inputtext[]);
public FactionDialog_Member_Edit(playerid, dialogid, response, listitem, string:inputtext[]) {
	
	new pFaction = GetPlayerFaction(playerid)-1,
		ply = GetPVarInt(playerid, "EditedPlayer")-1;
	if(!LoggedIn(ply) || GetPlayerFaction(ply) != pFaction+1 || !response || ply == playerid) {
		FactionDialog_Main(playerid, dialogid, response, listitem, "Members online");
		return 1;
	}
	if(listitem == 0) { // kick
		new string[OUTPUT];
		format(string, sizeof(string), "Are you sure you want to kick %s out of your faction?", GetPlayerNameEx(ply));
		CreateConfirmationDialog(playerid, string, "FactionDialog_KickPlayer", ply);
	}
	return 1;
}

forward FactionDialog_InvitationAnswer(playerid, ply, response);
public FactionDialog_InvitationAnswer(playerid, ply, response) {
	if(!response) {
		return 0;
	}
	new pFaction = GetPlayerFaction(playerid)-1;
	SetPVarInt(ply, "Org", pFaction+1);
	SetPVarInt(ply, "Rank", 1);
	return 1;
}

forward FactionDialog_Invitation(playerid, ply, response);
public FactionDialog_Invitation(playerid, ply, response) {
	new pFaction = GetPlayerFaction(playerid)-1;
	if(!response) {
		Command_ReProcess(playerid, "/faction", 0);
		return 1;
	}
	if(!IsPlayerConnected(ply) || ply == playerid) {
		Text_Send(playerid, $TARGET_NOT_LOGGED);
		return 1;
	}
	new string[OUTPUT], string2[OUTPUT*2];
	format(string, sizeof(string), "You have just invited %s (%d) to join your faction %s", GetPlayerNameEx(ply), ply, FactionInfo[pFaction][fName]);
	format(string2, sizeof(string2), "%s (%d) has just invited you to join their faction %s. Do you you want to join? You will leave your current faction if you are a member of one", GetPlayerNameEx(playerid), ply, FactionInfo[pFaction][fName]);
	CreateOffer(playerid, ply, string, string2, "FactionDialog_InvitationAnswer");
	return 1;
}

forward FactionDialog_Members(playerid, dialogid, response, listitem, string:inputtext[]);
public FactionDialog_Members(playerid, dialogid, response, listitem, string:inputtext[]) {
	new pFaction = GetPlayerFaction(playerid)-1, 
		pRank = GetPlayerRank(playerid)-1,
		title[128];
	if(!response || !strcmp(inputtext, "There are no faction members online", true) || FactionRankAccess[pFaction][pRank] < 10) {
		Command_ReProcess(playerid, "/faction", 0);
		return 1;
	}
	format(title, sizeof(title), "{00BB00}[Faction] {FFFFFF}%s - %s", FactionInfo[pFaction][fName], inputtext);
	if(!strcmp(inputtext, "Invite a member", true)) {
		ReturnUser(playerid, "Enter a name of a player you want to invite", "FactionDialog_Invitation");
	}
	else {
		new ply = INVALID_PLAYER_ID;
		sscanf(inputtext, "i", ply);
		if(!LoggedIn(ply) || GetPlayerFaction(ply) != pFaction+1) {
			FactionDialog_Main(playerid, dialogid, response, listitem, "Members online");
			return 1;
		}
		SetPVarInt(playerid, "EditedPlayer", ply+1);
		Dialog_ShowCallback(playerid, using callback FactionDialog_Member_Edit, DIALOG_STYLE_LIST, title, "Kick", "OK", "Back");
	}
	return 1;
}

forward FactionDialog_Ranks(playerid, dialogid, response, listitem, string:inputtext[]);
public FactionDialog_Ranks(playerid, dialogid, response, listitem, string:inputtext[]) {
	new pFaction = GetPlayerFaction(playerid)-1, 
		string[128], 
		title[128];
	if(!response) {
		Command_ReProcess(playerid, "/faction", 0);
		return 1;
	}
	SetPVarInt(playerid, "EditedRank", listitem);
	format(title, sizeof(title), "{00BB00}[Faction] {FFFFFF}%s - rank #%d", FactionInfo[pFaction][fName], listitem+1);
	format(string, sizeof(string), 
		"{00DD00}Title:{FFFFFF} %s\n\
		{00DD00}Salary:{FFFFFF} %d\n\
		{00DD00}Access level:{FFFFFF} %d/10",
		FactionRanks[pFaction][listitem], 
		FactionRankPaycheck[pFaction][listitem],
		FactionRankAccess[pFaction][listitem]);
	Dialog_ShowCallback(playerid, using callback FactionDialog_Rank_Edition, DIALOG_STYLE_LIST, title, string, "Edit", "Back");
	return 1;
}

forward FactionDialog_Main(playerid, dialogid, response, listitem, string:inputtext[]);
public FactionDialog_Main(playerid, dialogid, response, listitem, string:inputtext[]) {
	new pFaction = GetPlayerFaction(playerid)-1, 
		pRank = GetPlayerRank(playerid)-1,
		string[2048], 
		title[128];
	if(!response) { 
		return 1;
	}
	format(title, sizeof(title), "{00BB00}[Faction] {FFFFFF}%s", FactionInfo[pFaction][fName]);
	if(!strcmp(inputtext, "Help", true)) {
		Dialog_ShowCallback(playerid, using callback FactionDialog_Dummy, DIALOG_STYLE_MSGBOX, "Help", FACTION_HELP_TEXT, "OK", "");
		return 1;
	}
	else if(!strcmp(inputtext, "Members online", true)) {
		strcat(title, " - online members");
		foreach(new ply : Player) {
			if(GetPlayerFaction(ply) == pFaction+1) { 
				format(string, sizeof(string), "%s%2d %s\n", string, ply, GetPlayerNameEx(ply, false));
			}
		}
		strmid(string, string, 0, strlen(string)-1);
		if(!strlen(string))
			strcat(string, "\nThere are no faction members online");
		if(FactionRankAccess[pFaction][pRank] == 10) {
			strcat(string, "\nInvite a member");
			Dialog_ShowCallback(playerid, using callback FactionDialog_Members, DIALOG_STYLE_LIST, title, string, "Select", "Back");
		}
		else {
			Dialog_ShowCallback(playerid, using callback FactionDialog_Members, DIALOG_STYLE_LIST, title, string, "OK", "");
		}
	}
	else if(!strcmp(inputtext, "Faction info", true)) {
		strcat(title, " - information");
		format(string, sizeof(string), 
			"{00DD00}Name: {FFFFFF}%s\n\
			{00DD00}Type: {FFFFFF}%s\n\
			{00DD00}Creator: {FFFFFF}%s\n\
			{00DD00}Level: {FFFFFF}%d/10\n\
			{00DD00}Points: {FFFFFF}%d\n\
			{00DD00}Bank: {FFFFFF}%d\n\
			{00DD00}Members: {FFFFFF}%d/%d\n", 
			FactionInfo[pFaction][fName],
			FactionTypes[FactionInfo[pFaction][fType]],
			FactionInfo[pFaction][fOwnerName],
			FactionInfo[pFaction][fLevel],
			FactionInfo[pFaction][fPoints],
			FactionInfo[pFaction][fBank],
			CountFactionMembersOnline(pFaction),
			FactionInfo[pFaction][fMembers]
		);
		Dialog_ShowCallback(playerid, using callback FactionDialog_Dummy, DIALOG_STYLE_LIST, title, string, "OK", "");
	}
	else if(!strcmp(inputtext, "Faction ranks", true)) {
		strcat(title, " - ranks");
		for(new i=0;i<MAX_RANK;i++) {
			format(string, sizeof(string), "%sRank #%2d: %12s\t| Access level: {33CCFF}%2d/10{FFFFFF} | Salary: {33FFCC}$%d\n", 
				string,
				i+1,
				FactionRanks[pFaction][i], 
				FactionRankAccess[pFaction][i],
				FactionRankPaycheck[pFaction][i]
			);
		}
		strmid(string, string, 0, strlen(string)-1);
		Dialog_ShowCallback(playerid, using callback FactionDialog_Ranks, DIALOG_STYLE_LIST, title, string, "Edit", "Back");
	}
	return 1;
}

YCMD:faction(playerid, params[], help) {
	new pRank = GetPlayerRank(playerid)-1, 
		pFaction = GetPlayerFaction(playerid)-1, 
		string[256],
		title[128],
		id = GetPVarInt(playerid, "ID");
	strcat(string, "Help\n");
	if(pFaction < MAX_FACTIONS && pFaction >= 0) {
		strcat(string, "Members online\n");
		strcat(string, "Faction info");

		printf("%d == %d || %d == %d", FactionRankAccess[pFaction][pRank], FACTION_RANK_ACCESS_MAX,  FactionInfo[pFaction][fOwner], id);
		if(FactionRankAccess[pFaction][pRank] == FACTION_RANK_ACCESS_MAX || FactionInfo[pFaction][fOwner] == id) {
			strcat(string, "\nFaction ranks");
		}
		format(title, sizeof(title), "{00BB00}[Faction] {FFFFFF}%s", FactionInfo[pFaction][fName]);
	}
	else {
		format(title, sizeof(title), "[Faction menu]");
	}
	Dialog_ShowCallback(playerid, using callback FactionDialog_Main, DIALOG_STYLE_LIST, title, string, "Select", "Close");
	return 1;
}