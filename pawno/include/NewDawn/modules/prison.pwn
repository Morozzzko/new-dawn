/**
 * Source file for prison module
 *
 * @version 1.0
 * @author Moroz <igorsmorozov@gmail.com>
 */

#if !defined prison_included
	#endinput
#endif

#include <YSI\y_text>

loadtext Core[Commands], Core[Errors], Factions[Messages], Factions[Errors];

forward LoadPrisonCells_Callback(query[], property, extraid, connectionHandle);

public LoadPrisonCells_Callback(query[], property, extraid, connectionHandle) {
	new rows, cols,
		string[256], tmp[prisonCellInfo], i = 0;
	cache_get_data(rows, cols, connectionHandle);
	for(i=0;i<rows;i++) {
		cache_get_field_content(i, "id", string, connectionHandle);
		tmp[pcID] = strval(string);
		cache_get_field_content(i, "PosX", string, connectionHandle);
		tmp[pcPosX] = floatstr(string);
		cache_get_field_content(i, "PosY", string, connectionHandle);
		tmp[pcPosY] = floatstr(string);
		cache_get_field_content(i, "PosZ", string, connectionHandle);
		tmp[pcPosZ] = floatstr(string);
		cache_get_field_content(i, "Interior", string, connectionHandle);
		tmp[pcInt] = strval(string);
		cache_get_field_content(i, "VirtualWorld", string, connectionHandle);
		tmp[pcVW] = strval(string);
		PrisonCellInfo[property][i] = tmp;
	}
	printf("[Property] Loaded %d prison cells for %s (SID:%d)", i, PropertyInfo[property][prDescript], PropertyInfo[property][prID]);
	return 1;
}

/**
 * Loads prison cells
 *
 * @param property ID
 */

stock LoadPrisonCells(property) {
	new string[OUTPUT];
	if(property < 0 || property >= P_COUNT || PropertyInfo[property][prType] != P_TYPE_PRISON)  {
		return;
	}
	format(string, sizeof(string), "SELECT * FROM `property_prison_cells` WHERE `property`='%d' LIMIT %d", PropertyInfo[property][prID], MAX_PRISON_CELLS);
	mysql_query_callback(property, string, "LoadPrisonCells_Callback", 0, MySQL, true);
	return;
}


forward CreatePrisonCell_Callback(query[], property, cell, connectionHandle);

public CreatePrisonCell_Callback(query[], property, cell, connectionHandle) {
	PrisonCellInfo[property][cell][pcID] = mysql_insert_id(connectionHandle);
	return 1;
}

/**
 * Creates a prison cell
 * 
 * @param int property
 * @param int cell index (0..9)
 * @param float X position
 * @param float Y position
 * @param float Z position
 * @param int interior
 * @param int virtual world
 * @return 0 if an invalid argument was specified, 1 otherwise
 */
stock CreatePrisonCell(property, cell, Float:PosX, Float:PosY, Float:PosZ, int, VirtualWorld) {
	new query[256];
	if(property < 0 || property >= P_COUNT || PropertyInfo[property][prType] != P_TYPE_PRISON) {
		return 0;
	}
	if(cell < 0 || cell > MAX_PRISON_CELLS-1) {
		return 0;
	}
	PrisonCellInfo[property][cell][pcPosX] = PosX;
	PrisonCellInfo[property][cell][pcPosY] = PosY;
	PrisonCellInfo[property][cell][pcPosZ] = PosZ;
	PrisonCellInfo[property][cell][pcInt] = int;
	PrisonCellInfo[property][cell][pcVW] = VirtualWorld;
	if(PrisonCellInfo[property][cell][pcID] != 0) {
		format(query, sizeof(query), "UPDATE `property_prison_cells` SET `PosX`='%f', `PosY`='%f', `PosZ`='%f', `Interior`='%d', `VirtualWorld`='%d' WHERE `id`='%d'", PosX, PosY, PosZ, int, VirtualWorld, PrisonCellInfo[property][cell][pcID]);
		mysql_query(query, MySQL);
	}
	else {
		format(query, sizeof(query), "INSERT INTO `property_prison_cells` VALUES ( NULL, '%d', '%f', '%f', '%f', '%d', '%d' )", PropertyInfo[property][prID], PosX, PosY, PosZ, int, VirtualWorld);
		mysql_query_callback(property, query, "CreatePrisonCell_Callback", cell, MySQL);
	}
	return 1;
}

/** 
 * Removes a prison cell
 *
 * @param int property index
 * @param int cell index
 * @return 0 if an invalid argument was specified, 1 otherwise
 */
stock RemovePrisonCell(property, cell) {
	new query[256], tmp[prisonCellInfo];
	if(property < 0 || property >= P_COUNT || PropertyInfo[property][prType] != P_TYPE_PRISON) {
		return 0;
	}
	if(cell < 0 || cell > MAX_PRISON_CELLS-1 || PrisonCellInfo[property][cell][pcID] == 0) {
		return 0;
	}
	format(query, sizeof(query), "DELETE FROM `property_prison_cells` WHERE `id`='%d'", PrisonCellInfo[property][cell][pcID]);
	mysql_query(query, MySQL, false);
	PrisonCellInfo[property][cell] = tmp;
	return 1;
}
// TODO: Prison confirmation - let player choose to stay in the prison or not to stay

forward PrisonConfirmation(playerid, response, extraid);
public PrisonConfirmation(playerid, response, extraid) {
	if(!response) {
		SetPVarInt(playerid, "JailCell", 0);
	}
	else {
		SetPVarInt(playerid, "JailCell", 0);
	}
	return 1;
}

timer PrisonTimer[1000]() {
	foreach(new playerid : Player) {
		if(!LoggedIn(playerid)) {
			continue;
		}
		new time = GetPVarInt(playerid, "JailTime");
		if(time <= gettime() && time != 0) {
			SetPVarInt(playerid, "JailTime", 0);
			CreateConfirmationDialogEx(playerid, "Your sentence is over. Do you want to stay at your current position or would you rather be teleported to the prison entrance?", "PrisonConfirmation", "Teleport", "Stay");
		}
	}
	return 1;
}

hook OnGameModeInit() {
	repeat PrisonTimer();
	return 1;
}

YCMD:prison(playerid, params[], help) {
	new pProperty = GetPlayerInProperty(playerid), option[16], cell,
		Float:X, Float:Y, Float:Z;
		
	if(sscanf(params, "s[16]i", option, cell) || help) {
		Text_Send(playerid, $COMMAND_SYNTAX, YCMD:prison, "<option> <cell>");
		Text_Send(playerid, $COMMAND_ARGUMENTS, "addcell removecell");
		return 1;
	}
	if(pProperty == -1 || PropertyInfo[pProperty][prType] != P_TYPE_PRISON) {
		Text_Send(playerid, $MUST_BE_INSIDE_A_PRISON);
		return 1;
	}
	if(cell < 1 || cell > MAX_PRISON_CELLS) {
		Text_Send(playerid, $COMMAND_ARGUMENT_RANGE_INT, "cell", 1, MAX_PRISON_CELLS);
		return 1;
	}
	if(!strcmp(option, "addcell", true)) {
		GetPlayerPos(playerid, X, Y, Z);
		CreatePrisonCell(pProperty, cell-1, X, Y, Z, GetPlayerInterior(playerid), GetPlayerVirtualWorld(playerid));
		return 1;
	}
	else if(!strcmp(option, "removecell", true)) {
		RemovePrisonCell(pProperty, cell-1);
		return 1;
	}
	return 1;
}

YCMD:arrest(playerid, params[], help) {
	new ply = INVALID_PLAYER_ID, time = 0, cell = 0, pProperty, 
		pRank = GetPlayerRank(playerid),
		pFaction = GetPlayerFaction(playerid);
	if(sscanf(params, "uii", ply, time, cell)) {
		Text_Send(playerid, $COMMAND_SYNTAX, YCMD:arrest, "<playerid> <time> <cell>");
		return 1;
	}
	if(!LoggedIn(ply)) {
		Text_Send(playerid, $TARGET_NOT_LOGGED);
		return 1;
	}
	if(GetPlayerAccessLevel(playerid) < 2 || GetPlayerFactionType(playerid) != FACTION_TYPE_POLICE) {
		Text_Send(playerid, $NOT_IN_FACTION_TYPE, FactionTypes[FACTION_TYPE_POLICE]);
		return 1;
	}
	if(time < 0 || time > 336) {
		Text_Send(playerid, $COMMAND_ARGUMENT_RANGE_INT, "time", 0, 336);
		return 1;
	}
	if(time == 0 && GetPlayerAccessLevel(playerid) < 4) {
		Text_Send(playerid, $ACCESS_RANK_MIN, 4);
		return 1;
	}
	pProperty = GetPlayerInProperty(ply);
	if(pProperty == -1 || PropertyInfo[pProperty][prType] != P_TYPE_PRISON) {
		Text_Send(playerid, $MUST_BE_INSIDE_A_PRISON);
		return 1;
	}
	if(GetDistance(playerid, ply) > 5.0) {
		Text_Send(playerid, $PLAYER_IS_TOO_FAR);
		return 1;
	}
	if(cell < 1 || cell > MAX_PRISON_CELLS || PrisonCellInfo[pProperty][cell-1][pcID] == 0) {
		Text_Send(playerid, $COMMAND_ARGUMENT_RANGE_INT, "cell", 1, MAX_PRISON_CELLS);
		return 1;
	}
	if(time < 24) { // if less than a day
		Text_Send(playerid, $PLAYER_GOT_PRISONDED_H, FactionRanks[pFaction][pRank], GetPlayerNameEx(playerid), GetPlayerNameEx(ply), time);
		Text_Send(ply, $PLAYER_GOT_PRISONDED_H, FactionRanks[pFaction][pRank], GetPlayerNameEx(playerid), GetPlayerNameEx(ply), time);
		time = gettime()+time*60;
	}
	else if(time >= 24) { 
		Text_Send(playerid, $PLAYER_GOT_PRISONDED_D, FactionRanks[pFaction][pRank], GetPlayerNameEx(playerid), GetPlayerNameEx(ply), floatround(time/24, floatround_floor));
		Text_Send(ply, $PLAYER_GOT_PRISONDED_D, FactionRanks[pFaction][pRank], GetPlayerNameEx(playerid), GetPlayerNameEx(ply), floatround(time/24, floatround_floor));
		time = gettime()+time*60;
	}
	else {
		Text_Send(playerid, $PLAYER_GOT_PRISONDED_A, FactionRanks[pFaction][pRank], GetPlayerNameEx(playerid), GetPlayerNameEx(ply));
		Text_Send(ply, $PLAYER_GOT_PRISONDED_A, FactionRanks[pFaction][pRank], GetPlayerNameEx(playerid), GetPlayerNameEx(ply));
	}
	SetPVarInt(ply, "JailTime", time);
	SetPVarInt(ply, "JailCell", PrisonCellInfo[pProperty][cell][pcID]);
	MovePlayer(ply, PrisonCellInfo[pProperty][cell][pcPosX], PrisonCellInfo[pProperty][cell][pcPosY], PrisonCellInfo[pProperty][cell][pcPosZ], 0.0, PrisonCellInfo[pProperty][cell][pcInt], PrisonCellInfo[pProperty][cell][pcVW]);
	return 1;
}